# ACLED Events data
The ACLED events data is used to map events to social media interactions.

Information about the data source:
- https://acleddata.com/#/dashboard
- https://acleddata.com/resources/general-guides/

## How to
- Download the csv: https://acleddata.com/data-export-tool/
- Move the downloaded csv artefacts folder `/base/acled_events/`. E.g.
  `s3://phoenix-data-lake-prod/base/acled_events/`
- Run the ACLED CLI command: `./phoenix-cli events run production $DEFAULT_TENANT_ID acled`
- This will create: `final/acled_events/persisted.parquet` and will process all the files that have
  been added to the `base` folder.
- To add further data download again and add to `base` folder. The processing into final will
  de-duplicated by the `timestamp` so the most recent data will be kept.
