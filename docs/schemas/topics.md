# Topics dataframe

The topics is the dataframe topics computed for the objects.

| Column                | dtype   | Description |
|-----------------------|---------| ------------|
| object_id             | object  | The unique object id |
| object_type           | object  | Enum based on `phoenix/tag/data_pull/constants.py` |
| topic                 | object  | Computed topic for the text |
| matched_features      | object  | a list of string that are features the matched for the topic or
it can also be `custom_model_{key of the custom_model}` when a custom model classified it.|
| has_topic             | bool    | If this topic is a fill topic or not |
