# Tweets dataframe

Final tweets dataframe.

Data is mapped from `objects`, `topics` and data from twitter API: https://developer.twitter.com/en/docs/labs/tweets-and-users/api-reference/get-tweets-id.

| Column                    | dtype               | Description |
|---------------------------|---------------------|-------------|
| id                        | string              | ID |
| text                      | object              | Text of tweets |
| author_id                 | string              | Id of the author |
| conversation_id           | string              | Id of the conversation |
| language_from_api         | string              | language given from the twitter api |
| in_reply_to_user_id       | list<item: struct<id: string, type: string>> | - |
| retweet_count             | int64               | The number of times this tweet was retweeted |
| like_count                | int64               | The number of time this tweet was liked |
| quote_count               | int64               | The number of time this tweet was quoted |
| reply_count               | int64               | The number of time this tweet was replied to |
| bookmark_count            | int64               | The number of time this tweet was bookmarked |
| impression_count          | int64               | The number of time this tweet had an impression |
| retweeted                 | bool                | If the tweet is an retweet includes unofficial |
| medium_type               | string              | text or link |
| platform_media_type       | string              | text or link |
| url_within_text           | string              | First URL in the text |
| possibly_sensitive        | boolean             | If the text contains sensitive data |
| created_at                | datetime64[ns, UTC] | UTC timestamp that the tweet was created |
| timestamp_filter          | datetime64[ns, UTC] | Normalised column for filtering by timestamp. UTC timestamp that the tweet was created |
| file_timestamp            | datetime64[ns, UTC] | Timestamp of scrape |
| date_filter               | object (date32[day] in parquet) | Normalised column for filtering by date. UTC timestamp that the tweet was created |
| year_filter               | int64               | Normalised column for filtering by year. Year that the tweet was created |
| month_filter              | int64               | Normalised column for filtering by month. Month that the tweet was created |
| day_filter                | int64               | Normalised column for filtering by day. Day of the month that the tweet was created |

## Additional columns with the tensions inference
see (`objects_tensions`)[docs/schemas/objects.md#Objects_tensions]

# Tweets topics

This has the same data as tweets dataframe but has the columns as below.

There can be multiple topics for a tweets in this case the tweets data is repeated.

| Column                  | dtype          | Description |
|-------------------------|----------------|-------------|
| topic                   | object         | topic that phoenix found in the text |
