# Facebook
This document outlines the how and what sources are scrape by phoenix.

The sources that can be scraped and processed are:
- Facebook posts
- Facebook comments

## Facebook Posts
The facebook posts are scraped using the CrowdTangle API. 
This pulls data from facebook and requires permission from CrowdTangle to use.
This guide does not cover how to get access to CrowdTangle. 

The Facebook post data will come from a `list` on Crowdtangle.
Follow the Crowdtangle documentation to set up this list.
We use the `list IDs` to identify which list to pull on Crowdtangle.
These should be set on a per tenant basis, see `phoenix/config/tenants_template.yaml`.

Set the following environment variables:

`CROWDTANGLE_API_TOKEN`

`CT_RATE_LIMIT_CALLS` _(unless using default rate limit)_


To run the scrape, use the following command:
```bash 
$ ./phoenix-cli fb $(date --utc --iso-8601=seconds)
```

The facebook posts source run will pull the posts for the previous 3 days from the CrowdTangle API.
Previous 3 days because this is a balance between size of data pulled and accuracy of interactions with the post.

Data on the interactions on a post such as likes and comments change over time. In general the interactions change only slightly after 3 days.

Pulling only three days of data means that we are within the limits of what Facebook allows us to request.

The scraped data will conform to the following [Facebook posts schema](docs/facebook_posts_table.md) within Athena.

### CrowdTangle API
Facebook provides a [CrowdTangle API cheatsheet](https://help.crowdtangle.com/en/articles/3443476-api-cheat-sheet).
Here is the full [CrowdTangle API documentation](https://github.com/CrowdTangle/API/wiki).

#### Rate Limits
CrowdTangle has a base rate limit of 6 calls / minute.
We recommend asking to increase your Crowdtangle API rate limit with [a form on their site](https://www.facebook.com/help/contact/908993259530156).

We have found success with a limit of 30 calls / minute.

## Facebook comments
The facebook comments pipeline is more complex and includes a mix of manual and automatic processes.

The process has the ordered steps:

**1. Pull facebook posts for a month** - run the Facebook post process.

**2. Find top 10% of relevant posts by doing a tagging run** - 
The current implementation outputs a `posts_to_scrape.csv`, see [notebook](/phoenix/tag/twitter_facebook_posts_finalise.ipynb).

**3. Do a manual collection on the comments for each post** - 
following this tutorial on [collecting comments from Facebook pages](docs/facebook-comment-collection.md). 

**4. Upload all collected html pages to the proper `fb_comments/to_parse` cloud folder.** 

**5. Run a script that will process the html pages** and transform them into more structured data (json) using the following terminal command:
```bash
$ ./phoenix-cli fb-comments $(date --utc --iso-8601=seconds)
```

**6. Send the structured comments data through the tagging pipeline** so that the heuristics about that data can also be found. 
Here is [documentation on the Facebook Comments schema](docs/facebook_comments_table.md).

### Disclaimer

This approach to collecting Facebook comments is experimental and not officially sanctioned by Facebook. 
Use your own discretion when following this methodology.
If this approach is abused, it could result in blocked accounts or other forms of reprimand from Facebook.
While we have tested this approach in our own projects and found it to be valuable and viable, 
the Phoenix team is not responsible for blocked accounts. 

## Data Protection
It is up to you to follow the appropriate data protection guidelines as laid out in your jurisdiction.
