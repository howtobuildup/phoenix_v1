# Scraping

Phoenix supports the scraping of a number of platforms:
- Facebook -> through crowdtangle
- X/Twitter -> through twitter API v2
- YouTube -> though API

Phoenix takes no responsibility for how the scraping is done and it is the users responsibility to
collect data in the appropriate way.

## Experimental techniques
There are a number of experimental scraping techniques that includes facebook comments.

## Data Protection
It is up to you to follow the appropriate data protection guidelines as laid out in your jurisdiction.
