## Tweets
Tweets can be gathered from the twitter APIs using the user timeline.

You must have a Twitter developer account and then get the (paid) pro account. For more information
on the Twitter API, see [Twitter API documentation](https://developer.twitter.com/en/docs).

These tools are designed within the boundaries of the API guidelines,
and the Phoenix team is not responsible for any misuse.

### Configuration
A configuration CSV file is used for the scraping:

- `config/twitter_query_users.csv` - user handles to scrape, a new line per user handle

For local development these should be stored in `local_artifacts/config/`.
For production these should be stored in `<production url>/config/`.

### Authentication
There are two methods of authentication. As suggested in the `tweepy` documentation found
[here](https://docs.tweepy.org/en/v4.4.0/client.html) use OAuth 1 or 2. With OAuth 2 (Bearer token)
being the simplest.

The keys and tokens can be created in the developer platform of twitter,
[here](https://developer.twitter.com/en/portal/dashboard).
Once you have a subscription to developer pro account create a project. For that
project you can then create tokens needed for the OAuth method. See below for the naming
relationships.

For OAuth 2 use environment variables:
- `TWITTER_BEARER_TOKEN`: Named in twitter as "Authentication Tokens" -> "Bearer Token"

OAuth 2 is the preferred method and if `TWITTER_BEARER_TOKEN` is set phoenix will be use it to
authenticate over OAuth 1.

For OAuth 1 use environment variables:
-`TWITTER_CONSUMER_KEY`: Named in twitter as "Consumer Keys" -> "Key"
-`TWITTER_CONSUMER_SECRET`: Named in twitter as "Consumer Keys" -> "Secret"
-`TWITTER_OAUTH_ACCESS_TOKEN`: Named in twitter as "Authentication Tokens" -> "Access Token"
-`TWITTER_OAUTH_ACCESS_SECRET`: Named in twitter as "Authentication Tokens" -> "Access Secret"

OAuth 1 is used for giving your app permissions that match a user. Generally it is not needed.

### CLI
To see information on cli:
`./phoenix-cli scrape tweets --help`

### General
A source run collects the previous 3 days tweets.
This is in line with our approach for Facebook.
Our expectation is that most activity around individual tweets reduces after the first few days.

The Twitter data will conform to the following [Tweet schema](docs/schemas/tweets.md).
