import glob

import pytest

from phoenix.job_config import base, load


def pytest_generate_tests(metafunc):
    """Generate tests for all the valid and invalid configs."""
    invalid_configs = glob.glob("tests/unit/job_config/test_invalid_configs/*.yaml")
    valid_configs = glob.glob("tests/unit/job_config/test_valid_configs/*.yaml")

    if "invalid_config" in metafunc.fixturenames:
        metafunc.parametrize("invalid_config", invalid_configs)
    if "valid_config" in metafunc.fixturenames:
        metafunc.parametrize("valid_config", valid_configs)


def test_load_invalid_config(invalid_config):
    """Test processing an invalid config file."""
    with pytest.raises(ValueError):
        load.load_from_local_file(invalid_config)


def test_load_valid_config(valid_config):
    """Test processing a valid config file."""
    test_config = load.load_from_local_file(valid_config)
    assert isinstance(test_config, base.JobConfig)
