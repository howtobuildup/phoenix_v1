"""Unit tests for custom_models."""
import mock
import pandas as pd
import pytest

from phoenix import custom_models
from phoenix.custom_models import dummy_classifier, typing
from phoenix.tag.topic import single_feature_match as sfm


def test_get_model_not_found():
    """Test get model when key is not found."""
    with pytest.raises(ValueError):
        _ = custom_models.get_model("not_found", "")


def test_create_custom_classifiers_dict():
    """Test create_custom_classifiers_dict."""
    test_model_dict = {
        "test_classifier": mock.MagicMock(),
        "test_classifier_1": mock.MagicMock(),
    }
    base_custom_model_url = "base/"
    with mock.patch("phoenix.custom_models.MODEL_DICT", test_model_dict):
        classifiers_dict = custom_models.create_custom_classifiers_dict(
            custom_models_config=[
                {
                    "key": "test_classifier",
                },
                {
                    "key": "test_classifier_1",
                },
            ],
            base_custom_model_url=base_custom_model_url,
        )
        assert "test_classifier" in classifiers_dict
        assert "test_classifier_1" in classifiers_dict
        assert (
            classifiers_dict["test_classifier"] == test_model_dict["test_classifier"].return_value
        )
        assert (
            classifiers_dict["test_classifier_1"]
            == test_model_dict["test_classifier_1"].return_value
        )
        test_model_dict["test_classifier"].assert_called_once_with(base_custom_model_url)
        test_model_dict["test_classifier_1"].assert_called_once_with(base_custom_model_url)


def test_create_custom_classifiers_dict_no_key_in_config():
    """Test create_custom_classifiers_dict when there is no key in the config."""
    with pytest.raises(ValueError):
        _ = custom_models.create_custom_classifiers_dict(
            custom_models_config=[
                {
                    "name": "test_classifier",
                },
            ],
            base_custom_model_url="base/",
        )


def test_dummy_classifier_class_coloumns():
    """Test dummy classifier."""
    dummy_config = dummy_classifier.DEFAULT_DUMMY_CONFIG
    dummy = dummy_classifier.DummyClassifier(dummy_config)

    classification_columns = dummy.classification_columns()
    assert classification_columns == list(dummy_config.keys())


def test_run_predict():
    """Test run_predict."""
    test_model_dict = {
        "test_classifier": mock.MagicMock(),
        "test_classifier_1": mock.MagicMock(),
    }
    objects_df = mock.MagicMock()
    predictions_dict = custom_models.run_predict(
        # igrnoring the type because of the mock
        classifiers_dict=test_model_dict,  # type: ignore
        objects_df=objects_df,
    )
    assert "test_classifier" in predictions_dict
    assert "test_classifier_1" in predictions_dict
    assert (
        predictions_dict["test_classifier"]
        == test_model_dict["test_classifier"].predict.return_value
    )
    assert (
        predictions_dict["test_classifier_1"]
        == test_model_dict["test_classifier_1"].predict.return_value
    )
    test_model_dict["test_classifier"].predict.assert_called_once_with(objects_df)
    test_model_dict["test_classifier_1"].predict.assert_called_once_with(objects_df)


def test_predictions_to_topics_df():
    """Test predictions_to_topics_df forms the correct data frame."""
    objects_df = pd.DataFrame(
        {
            "object_id": ["o1", "o2", "o3", "o4"],
            "object_type": ["ot1", "ot1", "ot2", "ot2"],
            "objects_col": ["i", "i", "i", "i"],
        }
    )

    c1 = mock.Mock()
    c1.classification_columns.return_value = ["is_class_1", "class_1"]

    c2 = mock.Mock()
    c2.classification_columns.return_value = ["is_class_2", "class_2"]

    # Need to set type for the mocks
    classifiers_dict: dict[str, typing.CustomModel] = {
        "c1": c1,
        "c2": c2,
    }

    predictions_dict = {
        "c1": pd.DataFrame(
            {
                "object_id": ["o1", "o2", "o3", "o4"],
                "object_type": ["ot1", "ot1", "ot2", "ot2"],
                "is_class_1": [True, True, False, False],
                "is_not_class": [True, True, False, True],  # Should be ignored by the function
                "class_1": [True, True, True, False],
                "not_a_class": [1, 2, 3, 4],  # Should be ignored by the function
            }
        ),
        "c2": pd.DataFrame(
            {
                "object_id": ["o1", "o2", "o3", "o4"],
                "object_type": ["ot1", "ot1", "ot2", "ot2"],
                "is_class_2": [True, True, True, False],
                "is_not_class": [True, True, False, True],
                "class_2": [False, False, False, False],
            }
        ),
    }

    objects_classes_df = custom_models.predictions_to_topics_df(
        objects_df, classifiers_dict, predictions_dict
    )

    pd.testing.assert_frame_equal(
        objects_classes_df,
        pd.DataFrame(
            {
                "object_id": ["o1", "o1", "o1", "o2", "o2", "o2", "o3", "o3", "o4"],
                "topic": [
                    # o1
                    "class_1",
                    "is_class_1",
                    "is_class_2",
                    # o2
                    "class_1",
                    "is_class_1",
                    "is_class_2",
                    # o3
                    "class_1",
                    "is_class_2",
                    sfm.FILL_TOPIC,
                ],
                "matched_features": [
                    # o1
                    ["custom_model_c1"],
                    ["custom_model_c1"],
                    ["custom_model_c2"],
                    # o2
                    ["custom_model_c1"],
                    ["custom_model_c1"],
                    ["custom_model_c2"],
                    # o3
                    ["custom_model_c1"],
                    ["custom_model_c2"],
                    None,
                ],
                "object_type": ["ot1", "ot1", "ot1", "ot1", "ot1", "ot1", "ot2", "ot2", "ot2"],
                "has_topic": [True, True, True, True, True, True, True, True, False],
            }
        ),
    )
