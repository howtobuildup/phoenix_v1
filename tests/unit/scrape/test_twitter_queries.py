"""Test the twitter query functionality."""
import os

import mock
import pytest

from phoenix.scrape import twitter_queries


@mock.patch.dict(
    os.environ,
    {
        twitter_queries.ENV_BEARER_TOKEN: "bearer_token",
        twitter_queries.ENV_CONSUMER_KEY: "consumer_key",
        twitter_queries.ENV_CONSUMER_SECRET: "consumer_secret",
        twitter_queries.ENV_OAUTH_ACCESS_TOKEN: "oauth_access_token",
        twitter_queries.ENV_OAUTH_ACCESS_SECRET: "oauth_access_secret",
    },
    clear=True,
)
@mock.patch("tweepy.Client")
def test_get_client_oauth2bearerhandler(m_client):
    """Test the is OAuth2BearerHandler."""
    auth = twitter_queries.get_client()
    assert auth == m_client.return_value
    m_client.assert_called_once_with("bearer_token", wait_on_rate_limit=True)


@mock.patch.dict(
    os.environ,
    {
        twitter_queries.ENV_CONSUMER_KEY: "consumer_key",
        twitter_queries.ENV_CONSUMER_SECRET: "consumer_secret",
        twitter_queries.ENV_OAUTH_ACCESS_TOKEN: "oauth_access_token",
        twitter_queries.ENV_OAUTH_ACCESS_SECRET: "oauth_access_secret",
    },
    clear=True,
)
@mock.patch("tweepy.Client")
def test_get_client_oauth1bearerhandler(m_client):
    """Test the get client is Oauth1.0a."""
    auth = twitter_queries.get_client()
    assert auth == m_client.return_value
    assert auth == m_client.return_value
    m_client.assert_called_once_with(
        consumer_key="consumer_key",
        consumer_secret="consumer_secret",
        access_token="oauth_access_token",
        access_token_secret="oauth_access_secret",
        wait_on_rate_limit=True,
    )


@mock.patch.dict(
    os.environ,
    {},
    clear=True,
)
@mock.patch("tweepy.Client")
def test_get_client_error(m_client):
    """Test get_client with no environment keys."""
    with pytest.raises(RuntimeError) as error:
        twitter_queries.get_client()
    assert "not correct" in str(error.value)
    m_client.assert_not_called()
