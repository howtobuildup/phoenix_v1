"""Test acled_event."""
import datetime

import pandas as pd

from phoenix.common import utils
from phoenix.scrape import acled_event


def test_from_csvs():
    """Test the from_csvs."""
    rel_path = utils.relative_path("./", __file__)
    BASE_URL_ACLED_EVENTS_INPUT = f"file://{rel_path}/"
    event_data = acled_event.from_csvs(BASE_URL_ACLED_EVENTS_INPUT)
    assert event_data.shape[0] == 2
    pd.testing.assert_series_equal(
        event_data["event_id_cnty"],
        pd.Series(["NLD2538", "NLD2539"], name="event_id_cnty"),
    )
    # Testing the timestamp_filter processing is correct
    pd.testing.assert_series_equal(
        event_data["timestamp_filter"],
        pd.Series(
            [
                datetime.datetime(2023, 7, 30, tzinfo=datetime.timezone.utc),
                datetime.datetime(2023, 8, 2, tzinfo=datetime.timezone.utc),
            ],
            name="timestamp_filter",
        ),
    )

    # NLD2539 has a change in the second file
    assert event_data[event_data["event_id_cnty"] == "NLD2539"]["sub_event_type"].tolist() == [
        "CHANGED"
    ]
