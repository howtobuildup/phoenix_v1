"""Test forming run params for tweets scrape."""
import datetime
import os

import mock
import pytest
from freezegun import freeze_time

from phoenix.common.artifacts import registry_environment
from phoenix.scrape.run_params import tweets


URL_PREFIX = "s3://data-lake/"
ARTIFACTS_ENVIRONMENT_KEY = "production"
TENANT_ID = "tenant_id_1"
QUERY_TYPE = "user"


@freeze_time("2000-01-1 01:01:01", tz_offset=0)
@mock.patch.dict(os.environ, {registry_environment.PRODUCTION_ENV_VAR_KEY: URL_PREFIX})
@pytest.mark.parametrize(
    (
        "scrape_since_days, expected_scrape_since_days"
        ", scrape_start_date, expected_scrape_start_date"
        ", scrape_end_date, expected_scrape_end_date"
        ", limit_per_query, expected_limit_per_query"
        ", query_type"
    ),
    [
        (
            None,
            3,
            None,
            datetime.datetime(1999, 12, 29, 1, 1, 1, tzinfo=datetime.timezone.utc),
            None,
            datetime.datetime(2000, 1, 1, 1, 1, 1, tzinfo=datetime.timezone.utc),
            None,
            None,
            "user",
        ),
        (
            "4",
            4,
            None,
            datetime.datetime(1999, 12, 28, 1, 1, 1, tzinfo=datetime.timezone.utc),
            None,
            datetime.datetime(2000, 1, 1, 1, 1, 1, tzinfo=datetime.timezone.utc),
            "1",
            1,
            "user",
        ),
        (
            4,
            4,
            None,
            datetime.datetime(1999, 12, 28, 1, 1, 1, tzinfo=datetime.timezone.utc),
            None,
            datetime.datetime(2000, 1, 1, 1, 1, 1, tzinfo=datetime.timezone.utc),
            4,
            4,
            "user",
        ),
        (
            "0",
            0,
            None,
            datetime.datetime(2000, 1, 1, 1, 1, 1, tzinfo=datetime.timezone.utc),
            None,
            datetime.datetime(2000, 1, 1, 1, 1, 1, tzinfo=datetime.timezone.utc),
            None,
            None,
            "user",
        ),
        (
            0,
            0,
            None,
            datetime.datetime(2000, 1, 1, 1, 1, 1, tzinfo=datetime.timezone.utc),
            None,
            datetime.datetime(2000, 1, 1, 1, 1, 1, tzinfo=datetime.timezone.utc),
            None,
            None,
            "user",
        ),
        (
            4,
            4,
            None,
            datetime.datetime(1999, 11, 27, 0, 0, 0, tzinfo=datetime.timezone.utc),
            "1999-12-01",
            datetime.datetime(1999, 12, 1, 0, 0, 0, tzinfo=datetime.timezone.utc),
            None,
            None,
            "user",
        ),
        (
            None,
            3,
            None,
            datetime.datetime(1999, 11, 28, 0, 0, 0, tzinfo=datetime.timezone.utc),
            "1999-12-01",
            datetime.datetime(1999, 12, 1, 0, 0, 0, tzinfo=datetime.timezone.utc),
            None,
            None,
            "user",
        ),
        (
            None,
            None,
            datetime.datetime(1999, 12, 28, 1, 1, 1, tzinfo=datetime.timezone.utc),
            datetime.datetime(1999, 12, 28, 1, 1, 1, tzinfo=datetime.timezone.utc),
            None,
            datetime.datetime(2000, 1, 1, 1, 1, 1, tzinfo=datetime.timezone.utc),
            None,
            None,
            "user",
        ),
        (
            None,
            None,
            "1999-11-01",
            datetime.datetime(1999, 11, 1, 0, 0, tzinfo=datetime.timezone.utc),
            "1999-12-01",
            datetime.datetime(1999, 12, 1, 0, 0, 0, tzinfo=datetime.timezone.utc),
            None,
            None,
            "user",
        ),
        (
            None,
            None,
            "1999-11-01",
            datetime.datetime(1999, 11, 1, 0, 0, tzinfo=datetime.timezone.utc),
            "1999-12-01",
            datetime.datetime(1999, 12, 1, 0, 0, 0, tzinfo=datetime.timezone.utc),
            None,
            None,
            "keyword",
        ),
    ],
)
def test_create(
    scrape_since_days,
    expected_scrape_since_days,
    scrape_start_date,
    expected_scrape_start_date,
    scrape_end_date,
    expected_scrape_end_date,
    limit_per_query,
    expected_limit_per_query,
    tenants_template_url_mock,
    query_type,
):
    """Test creating run params for scraping tweets."""
    run_params = tweets.create(
        artifacts_environment_key=ARTIFACTS_ENVIRONMENT_KEY,
        tenant_id=TENANT_ID,
        query_type=query_type,
        run_datetime_str=None,
        scrape_since_days=scrape_since_days,
        scrape_start_date=scrape_start_date,
        scrape_end_date=scrape_end_date,
        limit_per_query=limit_per_query,
    )
    assert run_params
    assert isinstance(run_params, tweets.TweetsScrapeRunParams)
    BASE = "s3://data-lake/tenant_id_1/"

    urls = run_params.urls
    expected_source = (
        f"{BASE}source_runs/2000-01-01/source-{query_type}_tweets-20000101T010101.000000Z.json"
    )
    assert urls.source == expected_source
    assert run_params.scrape_since_days == expected_scrape_since_days
    assert run_params.scrape_start_date == expected_scrape_start_date
    assert run_params.scrape_end_date == expected_scrape_end_date
    assert run_params.limit_per_query == expected_limit_per_query


@freeze_time("2000-01-1 01:01:01", tz_offset=0)
@mock.patch.dict(os.environ, {registry_environment.PRODUCTION_ENV_VAR_KEY: URL_PREFIX})
@pytest.mark.parametrize(
    ("scrape_since_days, scrape_start_date, scrape_end_date"),
    [
        (
            3,
            "1999-12-28",
            None,
        ),
        (
            "3",
            "1999-12-28",
            None,
        ),
    ],
)
def test_create_value_error_scraping_range(
    scrape_since_days,
    scrape_start_date,
    scrape_end_date,
    tenants_template_url_mock,
):
    """Test error thrown when scrape_since_days and scrape_start_date are both set."""
    with pytest.raises(ValueError) as e:
        tweets.create(
            artifacts_environment_key=ARTIFACTS_ENVIRONMENT_KEY,
            tenant_id=TENANT_ID,
            query_type=QUERY_TYPE,
            run_datetime_str=None,
            scrape_since_days=scrape_since_days,
            scrape_start_date=scrape_start_date,
            scrape_end_date=scrape_end_date,
        )
        assert "scrape_since_days" in str(e.value)
        assert "scrape_start_date" in str(e.value)


def test_create_value_error_query_type(
    tenants_template_url_mock,
):
    """Test error thrown when query_type is not valid."""
    with pytest.raises(ValueError) as e:
        tweets.create(
            artifacts_environment_key=ARTIFACTS_ENVIRONMENT_KEY,
            tenant_id=TENANT_ID,
            query_type="BAD_QUERY_TYPE",
            run_datetime_str=None,
        )
        assert "BAD_QUERY_TYPE" in str(e.value)
