"""Tests for twitter."""
import datetime

import mock
import pytest
from freezegun import freeze_time

from phoenix.scrape import twitter_queries


@mock.patch("phoenix.scrape.twitter_queries.get_tweets")
@mock.patch("phoenix.scrape.twitter_queries.get_user_ids")
def test_get_tweets_for_queries(m_get_user_ids, m_get_tweets):
    """Tests correct get_tweets_for_queries."""
    # Args input
    query_type = "user"
    user_name_list = ["user1", "user2"]
    since_days = 1
    # Client mocking
    client = mock.Mock()
    return_tweets = [mock.Mock(), mock.Mock()]
    user_id_list = ["1", "2"]
    m_get_user_ids.return_value = user_id_list
    m_get_tweets.return_value = return_tweets
    start_date = datetime.datetime.now()
    end_date = datetime.datetime.now() - datetime.timedelta(since_days)
    tweets = twitter_queries.get_tweets_for_queries(
        query_type, user_name_list, start_date, end_date, client=client
    )
    # Asserted behaviour
    m_get_user_ids.assert_called_once_with(user_name_list, client)
    calls = [
        mock.call(query_type, user_id_list[0], start_date, end_date, None, client),
        mock.call(query_type, user_id_list[1], start_date, end_date, None, client),
    ]
    # Asserted behavior
    m_get_tweets.assert_has_calls(calls)
    # Asserted output
    assert tweets == return_tweets * len(user_name_list)


TWEET_FIELDS = ["f1", "f2"]


@mock.patch("phoenix.scrape.twitter_queries.TWEET_FIELDS", TWEET_FIELDS)
@mock.patch("phoenix.scrape.twitter_queries._get_user_tweets")
def test_get_user_tweets(m_get_user_tweets):
    """Tests correct get_tweets."""
    # Args input
    query_type = "user"
    user_name = "user1"
    start_time = datetime.datetime.now()
    end_time = datetime.datetime.now() - datetime.timedelta(1)
    # Client mocking
    client = mock.Mock()
    limit = 1
    return_tweets = [mock.Mock(), mock.Mock()]
    m_get_user_tweets.return_value = return_tweets
    tweets = twitter_queries.get_tweets(
        query_type, user_name, start_time, end_time, client=client, limit=limit
    )
    # Asserted behaviour
    expected_params = {
        "id": user_name,
        "start_time": start_time.isoformat(),
        "end_time": end_time.isoformat(),
        "tweet_fields": "f1,f2",
    }
    m_get_user_tweets.assert_called_once_with(client, expected_params, limit)
    # Asserted output
    assert tweets == return_tweets


@mock.patch("phoenix.scrape.twitter_queries.TWEET_FIELDS", TWEET_FIELDS)
@mock.patch("phoenix.scrape.twitter_queries._get_search_recent_tweets")
def test_get_tweets_keyword_recent(m_search_recent):
    """Tests get_tweets uses the recent_tweets function."""
    # Args input
    query_type = "keyword"
    keyword = "keyword1"
    # Client mocking
    client = mock.Mock()
    limit = 1
    return_tweets = [mock.Mock(), mock.Mock()]

    m_search_recent.return_value = return_tweets
    start_time = datetime.datetime(2000, 1, 20, 1, 1, 1, tzinfo=datetime.timezone.utc)
    end_time = datetime.datetime(2000, 1, 23, 4, 5, 6, tzinfo=datetime.timezone.utc)
    frozen_dt_now = datetime.datetime(2000, 1, 23, 4, 5, 6, tzinfo=datetime.timezone.utc)
    with freeze_time(frozen_dt_now):
        tweets = twitter_queries.get_tweets(
            query_type, keyword, start_time, end_time, client=client, limit=limit
        )
    # Asserted behaviour
    expected_params = {
        "query": keyword,
        "start_time": start_time.isoformat(),
        "end_time": end_time.isoformat(),
        "tweet_fields": "f1,f2",
    }
    m_search_recent.assert_called_once_with(client, expected_params, limit)
    # Asserted output
    assert tweets == return_tweets


@mock.patch("phoenix.scrape.twitter_queries.TWEET_FIELDS", TWEET_FIELDS)
@mock.patch("phoenix.scrape.twitter_queries._get_search_all_tweets")
def test_get_tweets_keyword_all(m_search_all):
    """Tests get_tweets uses the all_tweets function."""
    # Args input
    query_type = "keyword"
    keyword = "keyword1"
    # Client mocking
    client = mock.Mock()
    limit = 1
    return_tweets = [mock.Mock(), mock.Mock()]

    m_search_all.return_value = return_tweets
    # Start time is more than 7 days ago
    start_time = datetime.datetime(2000, 1, 1, 1, 1, 1, tzinfo=datetime.timezone.utc)
    end_time = datetime.datetime(2000, 1, 23, 4, 5, 6, tzinfo=datetime.timezone.utc)
    frozen_dt_now = datetime.datetime(2000, 1, 23, 4, 5, 6, tzinfo=datetime.timezone.utc)
    with freeze_time(frozen_dt_now):
        tweets = twitter_queries.get_tweets(
            query_type, keyword, start_time, end_time, client=client, limit=limit
        )
    # Asserted behaviour
    expected_params = {
        "query": keyword,
        "start_time": start_time.isoformat(),
        "end_time": end_time.isoformat(),
        "tweet_fields": "f1,f2",
    }
    m_search_all.assert_called_once_with(client, expected_params, limit)
    # Asserted output
    assert tweets == return_tweets


def test_get_tweets_for_queries_fails_with_bad_query_type():
    """Tests incorrect query type for get_tweets_for_queries."""
    bad_query_type = "bad query type"
    queries = ["bad query"]
    since_days = 1
    start_date = datetime.datetime.now()
    end_date = datetime.datetime.now() - datetime.timedelta(since_days)
    with pytest.raises(ValueError):
        _ = twitter_queries.get_tweets_for_queries(
            bad_query_type, queries, start_date, end_date, client=None, queries_is_ids=False
        )


@pytest.mark.parametrize(
    "query_type,start_time,expected_get_user,expected_get_search_all,expected_get_search_recent",
    (
        (
            "user",
            datetime.datetime(2000, 1, 23, 4, 5, 6, tzinfo=datetime.timezone.utc),
            True,
            False,
            False,
        ),
        (
            "keyword",
            datetime.datetime(2000, 1, 23, 4, 5, 6, tzinfo=datetime.timezone.utc),
            False,
            False,
            True,
        ),
        (
            "keyword",
            datetime.datetime(2000, 1, 2, 3, 4, 5, tzinfo=datetime.timezone.utc),
            False,
            True,
            False,
        ),
    ),
)
@mock.patch("phoenix.scrape.twitter_queries._get_user_tweets")
@mock.patch("phoenix.scrape.twitter_queries._get_search_all_tweets")
@mock.patch("phoenix.scrape.twitter_queries._get_search_recent_tweets")
def test_get_tweet_iterator(
    m_search_recent,
    m_search_all,
    m_user_tweets,
    query_type,
    start_time,
    expected_get_user,
    expected_get_search_all,
    expected_get_search_recent,
):
    """Tests _get_tweet_iterator."""
    client = mock.Mock()
    params = {"param1": "param1"}
    limit = 10
    frozen_dt_now = datetime.datetime(2000, 1, 23, 4, 5, 6, tzinfo=datetime.timezone.utc)
    with freeze_time(frozen_dt_now):
        _ = twitter_queries._get_tweet_iterator(client, params, query_type, start_time, limit)

    if expected_get_user:
        m_user_tweets.assert_called_once_with(client, params, limit)
        m_search_all.assert_not_called()
        m_search_recent.assert_not_called()
    elif expected_get_search_all:
        m_user_tweets.assert_not_called()
        m_search_all.assert_called_once_with(client, params, limit)
        m_search_recent.assert_not_called()
    elif expected_get_search_recent:
        m_user_tweets.assert_not_called()
        m_search_all.assert_not_called()
        m_search_recent.assert_called_once_with(client, params, limit)
