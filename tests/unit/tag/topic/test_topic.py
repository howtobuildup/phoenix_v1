"""Test topic."""
import pandas as pd

from phoenix.tag import topic
from phoenix.tag.topic import single_feature_match as sfm


def test_get_object_topics():
    """Test get_object_topics."""
    topics_df = pd.DataFrame(
        {
            "object_id": ["o1", "o1", "o2", "o2", "o3"],
            "topic": ["t1", "t2", "t1", "t2", sfm.FILL_TOPIC],
            "object_type": ["ot1", "ot1", "ot1", "ot1", "ot2"],
            "matched_features": [["f1"], ["f1", "f2"], ["f1"], ["f1"], None],
            "has_topic": [True, True, True, True, False],
        }
    )

    objects_df = pd.DataFrame(
        {
            "object_id": ["o1", "o2", "o3"],
            "object_type": ["ot1", "ot1", "ot2"],
            "objects_col": ["i", "i", "i"],
        }
    )

    objects_topics_df = topic.get_object_topics(topics_df, objects_df)

    pd.testing.assert_frame_equal(
        objects_topics_df,
        pd.DataFrame(
            {
                "object_id": ["o1", "o2", "o3"],
                "object_type": ["ot1", "ot1", "ot2"],
                "objects_col": ["i", "i", "i"],
                "topics": [["t1", "t2"], ["t1", "t2"], [sfm.FILL_TOPIC]],
                "has_topics": [True, True, False],
            }
        ),
    )


def test_merge_topics():
    """Test merge_topics."""
    topics_1_df = pd.DataFrame(
        {
            "object_id": ["o1", "o1", "o2", "o2", "o3", "o4", "o5"],
            "topic": ["t1", "t2", "t1", "t2", sfm.FILL_TOPIC, sfm.FILL_TOPIC, sfm.FILL_TOPIC],
            "object_type": ["ot1", "ot1", "ot1", "ot1", "ot2", "ot2", "ot2"],
            "matched_features": [["f1"], ["f1", "f2"], ["f1"], ["f1"], None, None, None],
            "has_topic": [True, True, True, True, False, False, False],
        }
    )

    topics_2_df = pd.DataFrame(
        {
            "object_id": ["o1", "o1", "o2", "o2", "o3", "o4"],
            "topic": ["t1", "c1", "c1", "c2", "c2", sfm.FILL_TOPIC],
            "object_type": ["ot1", "ot1", "ot1", "ot1", "ot2", "ot2"],
            # This matches the sfm output
            "matched_features": [
                ["classifier1"],
                ["classifier2"],
                ["classifier2"],
                ["classifier2"],
                ["classifier2"],
                None,
            ],
            "has_topic": [True, True, True, True, True, False],
        }
    )

    topics_df = topic.merge_topics([topics_1_df, topics_2_df])

    pd.testing.assert_frame_equal(
        topics_df,
        pd.DataFrame(
            {
                "object_id": [
                    "o1",
                    "o1",
                    "o1",
                    "o2",
                    "o2",
                    "o2",
                    "o2",
                    "o3",
                    "o4",
                    "o5",
                ],
                "topic": [
                    # o1
                    "c1",
                    "t1",
                    "t2",
                    # o2
                    "c1",
                    "c2",
                    "t1",
                    "t2",
                    # o3
                    "c2",
                    # No topics
                    sfm.FILL_TOPIC,
                    sfm.FILL_TOPIC,
                ],
                "object_type": [
                    # o1
                    "ot1",
                    "ot1",
                    "ot1",
                    # o2
                    "ot1",
                    "ot1",
                    "ot1",
                    "ot1",
                    # o3
                    "ot2",
                    # No topics
                    "ot2",
                    "ot2",
                ],
                "matched_features": [
                    # o1
                    ["classifier2"],
                    # Two different matched_features have been joined
                    ["f1", "classifier1"],
                    ["f1", "f2"],
                    # o2
                    ["classifier2"],
                    ["classifier2"],
                    ["f1"],
                    ["f1"],
                    # o3
                    ["classifier2"],
                    # No topics
                    None,
                    None,
                ],
                "has_topic": [
                    # o1
                    True,
                    True,
                    True,
                    # o2
                    True,
                    True,
                    True,
                    True,
                    # o3
                    True,
                    # No topics
                    False,
                    False,
                ],
            }
        ),
    )
