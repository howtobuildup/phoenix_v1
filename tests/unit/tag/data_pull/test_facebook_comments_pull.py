import pandas as pd

from phoenix.tag.data_pull.facebook_comments_pull import get_comments_df


def test_get_comments_df_exportcomments():
    """Test get comments df."""
    pages = [
        {
            "Unnamed: 0": 102,
            "Unnamed: 1": None,
            "Name": "Jimmy Mack",
            "Profile ID": "ID: 100080315772454",
            "Date": "2023-04-24 11:46:03",
            "Likes": 0,
            "Stars": None,
            "Comment": "Well did some one get Chinese kick back????who???",
            "URL": "https://www.exportcomments.com/done/"
            "ba10e93b-bbb1-427d-8f2c-f06cafe5576f/preview/125122783883610",
            "post_id": "831722021643024",
            "source": "exportcomments",
        }
    ]
    result = pd.DataFrame(
        [
            {
                "id": "125122783883610",
                "post_id": "831722021643024",
                "file_id": "831722021643024",
                "parent_id": "831722021643024",
                "post_created": "2023-04-24 11:46:03",
                "text": "Well did some one get Chinese kick back????who???",
                "reactions": 0,
                "top_sentiment_reactions": None,
                "user_display_name": "Jimmy Mack",
                "user_name": "ID: 100080315772454",
                "position": "",
            }
        ]
    )
    pd.testing.assert_frame_equal(get_comments_df(pages), result)


def test_get_comments_df_pages():
    pages = [
        {
            "comments": [
                {
                    "id": "1234",
                    "post_id": "1234",
                    "file_id": "1234",
                    "parent": "1234",
                    "date_utc": "2023-04-24 11:46:03",
                    "text": "Well did some one get Chinese kick back????who???",
                    "reactions": 0,
                    "top_sentiment_reactions": None,
                    "user_display_name": "Jimmy Mack",
                    "user_name": "Jim",
                    "position": "first",
                }
            ]
        }
    ]
    result = pd.DataFrame(
        [
            {
                "id": "1234",
                "post_id": "1234",
                "file_id": "1234",
                "parent_id": "1234",
                "post_created": "2023-04-24 11:46:03",
                "text": "Well did some one get Chinese kick back????who???",
                "reactions": 0,
                "top_sentiment_reactions": None,
                "user_display_name": "Jimmy Mack",
                "user_name": "Jim",
                "position": "first",
            }
        ]
    )
    pd.testing.assert_frame_equal(get_comments_df(pages), result)


def test_normalise_exportcomments_instagram():
    pages = [
        {
            "Unnamed: 0": 102,
            "Unnamed: 1": None,
            "Name": "Jimmy Mack",
            "Profile ID": "ID: 100080315772454",
            "Date": "2023-04-24 11:46:03",
            # ExportComments does not export reactions(likes) for
            # instagram comments, it is always 0.
            "Likes": 0,
            "Comment": "Well did some one get Chinese kick back????who???",
            "URL": "https://www.exportcomments.com/done/"
            "ba10e93b-bbb1-427d-8f2c-f06cafe5576f/preview/125122783883610",
            "post_id": "831722021643024",
            "source": "exportcomments",
            "Comment ID": "555",
        }
    ]
    result = pd.DataFrame(
        [
            {
                "id": "555",
                "post_id": "831722021643024",
                "file_id": "831722021643024",
                "parent_id": "831722021643024",
                "post_created": "2023-04-24 11:46:03",
                "text": "Well did some one get Chinese kick back????who???",
                # ExportComments does not export reactions(likes) for
                # instagram comments, it is always 0.
                "reactions": None,
                "top_sentiment_reactions": None,
                "user_display_name": "Jimmy Mack",
                "user_name": "ID: 100080315772454",
                "position": "",
            }
        ]
    )
    pd.testing.assert_frame_equal(get_comments_df(pages), result)


def test_normalise_apify_facebook():
    """Test normalise_apify_facebook method."""
    pages = [
        {
            "commentUrl": "test_commentUrl",
            "commentsCount": 1,
            "date": "2024-01-07T12:52:29.000Z",
            "facebookId": "test_facebookId",
            "facebookUrl": "test_facebookUrl",
            "feedbackId": "test_feedbackId",
            "id": "test_id",
            "likesCount": 3,
            "pageAdLibrary/id": "test_id",
            "pageAdLibrary/is_business_page_active": "test_is_business_page_active",
            "postTitle": "test_postTitle",
            "profileId": "test_profileId",
            "profileName": "test_profileName",
            "profilePicture": "test_profilePicture",
            "profileUrl": "test_profileUrl",
            "redirectFromUrl": "test_redirectFromUrl",
            "replyToCommentId": "test_replyToCommentId",
            "text": "test_text",
            "threadingDepth": 1,
        }
    ]
    result = pd.DataFrame(
        [
            {
                "id": "test_id",
                "post_id": "test_facebookId",
                "parent_id": "test_facebookId",
                # Currently there's a bug in the apify json crawler that doesn't return the
                # replyToCommentId field. A bugreport is open, and once this is fixed we can
                # uncomment the next line and the position line.
                # "parent_id": "test_replyToCommentId",
                "post_created": "2024-01-07T12:52:29.000Z",
                "text": "test_text",
                "reactions": 3,
                "top_sentiment_reactions": None,
                "user_display_name": "test_profileName",
                "user_name": "test_profileId",
                "position": "",
                # "position": 1,
            }
        ]
    )
    pd.testing.assert_frame_equal(get_comments_df(pages), result)
