"""Test for_object_type and topics_for_object_type."""
import pandas as pd
import pytest

from phoenix.tag import finalise


def test_for_object_type_not_supported():
    """Test that error when unsupported object type."""
    object_type = "not_supported_type"
    empty_df = pd.DataFrame({})
    with pytest.raises(RuntimeError) as e:
        finalise.for_object_type(object_type, empty_df)
        assert object_type in str(e.value)


def test_topics_for_object_type_not_supported():
    """Test that error when unsupported object type."""
    object_type = "not_supported_type"
    empty_df = pd.DataFrame({})
    with pytest.raises(RuntimeError) as e:
        finalise.topics_for_object_type(object_type, empty_df, empty_df)
        assert object_type in str(e.value)


@pytest.fixture
def original_objects_for_join():
    """Original objects for the join."""
    return pd.DataFrame(
        {
            "id": ["1", "2", "3", "4"],
            "object_type": ["ot", "ot", "ot", "ot"],
            "url": ["url1", "url2", "url3", "url4"],
            "year_filter": [2021, 2021, 2021, 2021],
            "month_filter": [1, 1, 2, 2],
        }
    )


@pytest.fixture
def classifications_for_join():
    """Classifications for the join."""
    return pd.DataFrame(
        {
            "object_id": ["1", "2", "3", "4"],
            "topic": [["t_1", "t_2"], ["t_2"], [], ["t_4"]],
            "has_topic": [True, True, False, True],
            "text": ["text_1", "text_2", "text_3", "text_4"],
        }
    )


@pytest.fixture
def language_sentiment_objects_for_join():
    """Language Sentiment Objects for the join."""
    return pd.DataFrame(
        {
            "object_id": ["1", "2", "3", "4"],
            "language_sentiment": ["POSITIVE", "NEGATIVE", "POSITIVE", "POSITIVE"],
            "language_sentiment_score_mixed": [0.9, 0.9, 0.8, 0.8],
            "language_sentiment_score_neutral": [0.9, 0.9, 0.8, 0.8],
            "language_sentiment_score_negative": [0.9, 0.9, 0.8, 0.8],
            "language_sentiment_score_positive": [0.9, 0.9, 0.8, 0.8],
        }
    )


@pytest.mark.parametrize(
    "object_type", ["tiktok_comments", "tiktok_videos", "youtube_comments", "youtube_videos"]
)
def test_finalise_object_type_with_classifications_sentiment(
    original_objects_for_join,
    classifications_for_join,
    language_sentiment_objects_for_join,
    object_type,
):
    """Test the join of objects to classifications and sentiment analysis results."""
    result_df = finalise.for_object_type(
        object_type=object_type,
        df=original_objects_for_join,
        objects_df=classifications_for_join,
        language_sentiment_objects_df=language_sentiment_objects_for_join,
    )
    expected_df = pd.DataFrame(
        {
            "id": ["1", "2", "3", "4"],
            "object_type": ["ot", "ot", "ot", "ot"],
            "url": ["url1", "url2", "url3", "url4"],
            "year_filter": [2021, 2021, 2021, 2021],
            "month_filter": [1, 1, 2, 2],
            "topic": [["t_1", "t_2"], ["t_2"], [], ["t_4"]],
            "has_topic": [True, True, False, True],
            "text": ["text_1", "text_2", "text_3", "text_4"],
            "language_sentiment": ["POSITIVE", "NEGATIVE", "POSITIVE", "POSITIVE"],
            "language_sentiment_score_mixed": [0.9, 0.9, 0.8, 0.8],
            "language_sentiment_score_neutral": [0.9, 0.9, 0.8, 0.8],
            "language_sentiment_score_negative": [0.9, 0.9, 0.8, 0.8],
            "language_sentiment_score_positive": [0.9, 0.9, 0.8, 0.8],
        },
        index=pd.Index(["1", "2", "3", "4"], name="object_id"),
    )
    pd.testing.assert_frame_equal(result_df, expected_df)


@pytest.mark.parametrize(
    "object_type", ["tiktok_comments", "tiktok_videos", "youtube_comments", "youtube_videos"]
)
def test_join_object_type_to_sentiment_with_no_classifications(
    original_objects_for_join, language_sentiment_objects_for_join, object_type
):
    """Test the join of objects to sentiment with no classifications."""
    result_df = finalise.for_object_type(
        object_type=object_type,
        df=original_objects_for_join,
        language_sentiment_objects_df=language_sentiment_objects_for_join,
    )
    pd.testing.assert_frame_equal(
        result_df,
        pd.DataFrame(
            {
                "id": ["1", "2", "3", "4"],
                "object_type": ["ot", "ot", "ot", "ot"],
                "url": ["url1", "url2", "url3", "url4"],
                "year_filter": [2021, 2021, 2021, 2021],
                "month_filter": [1, 1, 2, 2],
                "language_sentiment": ["POSITIVE", "NEGATIVE", "POSITIVE", "POSITIVE"],
                "language_sentiment_score_mixed": [0.9, 0.9, 0.8, 0.8],
                "language_sentiment_score_neutral": [0.9, 0.9, 0.8, 0.8],
                "language_sentiment_score_negative": [0.9, 0.9, 0.8, 0.8],
                "language_sentiment_score_positive": [0.9, 0.9, 0.8, 0.8],
            },
            index=pd.Index(["1", "2", "3", "4"], name="object_id"),
        ),
    )


@pytest.mark.parametrize(
    "object_type", ["tiktok_comments", "tiktok_videos", "youtube_comments", "youtube_videos"]
)
def test_join_object_to_object_type_no_langauge_sentiment_objects_with_classification(
    original_objects_for_join, classifications_for_join, object_type
):
    """Test the join of objects to youtube_videos."""
    result_df = finalise.for_object_type(
        object_type=object_type,
        df=original_objects_for_join,
        objects_df=classifications_for_join,
    )
    pd.testing.assert_frame_equal(
        result_df,
        pd.DataFrame(
            {
                "id": ["1", "2", "3", "4"],
                "object_type": ["ot", "ot", "ot", "ot"],
                "url": ["url1", "url2", "url3", "url4"],
                "year_filter": [2021, 2021, 2021, 2021],
                "month_filter": [1, 1, 2, 2],
                "topic": [["t_1", "t_2"], ["t_2"], [], ["t_4"]],
                "has_topic": [True, True, False, True],
                "text": ["text_1", "text_2", "text_3", "text_4"],
            },
            index=pd.Index(["1", "2", "3", "4"], name="object_id"),
        ),
    )


@pytest.mark.parametrize(
    "object_type", ["tiktok_comments", "tiktok_videos", "youtube_comments", "youtube_videos"]
)
def test_join_object_type_no_classification_no_sentiment(original_objects_for_join, object_type):
    """Test the join of objects when there are no additional objects to join."""
    result_df = finalise.for_object_type(
        object_type=object_type,
        df=original_objects_for_join,
    )
    pd.testing.assert_frame_equal(
        result_df,
        pd.DataFrame(
            {
                "id": ["1", "2", "3", "4"],
                "object_type": ["ot", "ot", "ot", "ot"],
                "url": ["url1", "url2", "url3", "url4"],
                "year_filter": [2021, 2021, 2021, 2021],
                "month_filter": [1, 1, 2, 2],
            },
            index=pd.Index(["1", "2", "3", "4"], name="object_id"),
        ),
    )
