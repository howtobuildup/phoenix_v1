"""Unit tests for custom_models utils."""

import pandas as pd

from phoenix.custom_models import dummy_classifier, utils


def test_explode_str():
    expected_df = pd.DataFrame(
        {
            "a": ["hello", "world", "foo", "bar", "baz"],
            "b": ["string_1", "string_1", "string_2", "string_2", "string_2"],
        },
        index=[0, 0, 1, 1, 1],
    )
    input_df = pd.DataFrame(
        {"a": ["hello, world", "foo, bar, baz"], "b": ["string_1", "string_2"]}
    )
    output_df = utils.explode_str(input_df, "a", ",")

    pd.testing.assert_frame_equal(expected_df, output_df)


def test_load_persist_model_handels_folders(tmp_path):
    """Test persist model handles folders."""
    folder = str(tmp_path / "folder")
    folder_url = f"file://{folder}"
    dummy_config = dummy_classifier.DEFAULT_DUMMY_CONFIG
    dummy_to_persist = dummy_classifier.DummyClassifier(dummy_config)
    _ = utils.persist_model(dummy_to_persist, dummy_classifier.DEFAULT_FILE_NAME, folder_url)
    model = utils.load_model(
        dummy_classifier.DummyClassifier, dummy_classifier.DEFAULT_FILE_NAME, folder_url
    )
    assert isinstance(model, dummy_classifier.DummyClassifier)


def test_load_persist_model_handels_url_with_backslash(tmp_path):
    """Test persist model handles folders."""
    folder = str(tmp_path / "folder/")
    folder_url = f"file://{folder}"
    dummy_config = dummy_classifier.DEFAULT_DUMMY_CONFIG
    dummy_to_persist = dummy_classifier.DummyClassifier(dummy_config)
    _ = utils.persist_model(dummy_to_persist, dummy_classifier.DEFAULT_FILE_NAME, folder_url)
    model = utils.load_model(
        dummy_classifier.DummyClassifier, dummy_classifier.DEFAULT_FILE_NAME, folder_url
    )
    assert isinstance(model, dummy_classifier.DummyClassifier)
