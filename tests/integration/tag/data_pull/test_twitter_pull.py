"""Test Twitter tweets data puller (processing of raw scraped data into structured data)."""
import datetime

import pandas as pd
import pytest

from phoenix.tag import data_pull


@pytest.fixture()
def processed_df() -> pd.DataFrame:
    """Fixture for dataframe processed from scraped tweets JSON."""
    df = pd.DataFrame(
        [
            {
                "id": "1111111111111111111",
                "referenced_tweets": [{"type": "quoted", "id": "1444444444444444444"}],
                "text": "text. https://t.co/url",
                "language_from_api": "en",
                "possibly_sensitive": False,
                "conversation_id": "1333333333333333333",
                "author_id": "12222222",
                "created_at": datetime.datetime(
                    2023, 1, 1, 19, 18, 49, tzinfo=datetime.timezone.utc
                ),
                "coordinates": "[]",  # This is a string representation of an empty list
                # This is a string representation of a dict
                "place": """{'id': '07d9db48bc083000', 'name': 'Louisiana'}""",
                "in_reply_to_user_id": None,
                "file_timestamp": datetime.datetime(
                    2023, 2, 2, 1, 1, 1, 411707, tzinfo=datetime.timezone.utc
                ),
                "timestamp_filter": datetime.datetime(
                    2023, 1, 1, 19, 18, 49, tzinfo=datetime.timezone.utc
                ),
                "date_filter": datetime.date(2023, 1, 1),
                "year_filter": 2023,
                "month_filter": 1,
                "day_filter": 1,
                "retweet_count": 4,
                "reply_count": 0,
                "like_count": 20,
                "quote_count": 0,
                "bookmark_count": 0,
                "impression_count": 2162,
                "medium_type": "link",
                "platform_media_type": None,
                "url_within_text": "https://t.co/url",
                "retweeted": True,
            },
            {
                "id": "1111111111111111112",
                "referenced_tweets": None,
                "text": "text. https://t.co/url",
                "language_from_api": "en",
                "possibly_sensitive": False,
                "conversation_id": "1333333333333333333",
                "author_id": "12222222",
                "created_at": datetime.datetime(2023, 1, 1, 1, 1, 1, tzinfo=datetime.timezone.utc),
                "coordinates": "",  # This is a string representation of an na value
                "place": "",  # This is a string representation of an na value
                "in_reply_to_user_id": "13333333",
                "file_timestamp": datetime.datetime(
                    2023, 2, 2, 1, 1, 1, 411707, tzinfo=datetime.timezone.utc
                ),
                "timestamp_filter": datetime.datetime(
                    2023, 1, 1, 1, 1, 1, tzinfo=datetime.timezone.utc
                ),
                "date_filter": datetime.date(2023, 1, 1),
                "year_filter": 2023,
                "month_filter": 1,
                "day_filter": 1,
                "retweet_count": 4,
                "reply_count": 1,
                "like_count": 20,
                "quote_count": 0,
                "bookmark_count": 0,
                "impression_count": 2162,
                "medium_type": "text",
                "platform_media_type": None,
                "url_within_text": None,
                "retweeted": False,
            },
        ]
    )
    df["coordinates"] = df["coordinates"].astype("string")
    df["place"] = df["place"].astype("string")
    return df


def test_data_pull(twitter_raw_data_source_folder_url, processed_df):
    """Integration test for data pull (processing) of twitter files."""
    result = data_pull.twitter_pull.twitter_json(twitter_raw_data_source_folder_url)
    pd.testing.assert_frame_equal(result, processed_df)
