"""Test Tiktok videos data puller."""
import datetime

import pandas as pd
import pytest

from phoenix.tag import data_pull
from phoenix.tag.data_pull import tiktok_videos_pull as tiktok_videos


@pytest.fixture()
def processed_videos_df() -> pd.DataFrame:
    """Fixture for dataframe processed from scraped videos JSON."""
    df = pd.DataFrame(
        [
            {
                "id": "1234567890000000000",
                "created_at": datetime.datetime(
                    2024, 3, 10, 15, 30, 26, tzinfo=datetime.timezone.utc
                ),
                "author_id": "1111111111111111111",
                "video_url": "https://www.tiktok.com/@yanatheartist/video/7344757393005268267",
                "author_url": tiktok_videos.TIKTOK_AUTHOR_URL + "yanatheartist",
                "text": (
                    "Content Break- Tika started deleting all my comments 😃 #goviralgo"
                    " #beautyhacks #hudabeauty "
                ),
                "like_count": 124,
                "share_count": 4,
                "impression_count": 854,
                "collect_count": 8,
                "comment_count": 4,
                "timestamp_filter": datetime.datetime(
                    2024, 3, 10, 15, 30, 26, tzinfo=datetime.timezone.utc
                ),
                "date_filter": datetime.date(2024, 3, 10),
                "year_filter": 2024,
                "month_filter": 3,
                "day_filter": 10,
                "file_timestamp": datetime.datetime(
                    2024, 3, 11, 4, 7, 46, 887000, tzinfo=datetime.timezone.utc
                ),
            },
            {
                "id": "9876543210000000000",
                "created_at": datetime.datetime(
                    2024, 3, 10, 0, 37, 59, tzinfo=datetime.timezone.utc
                ),
                "author_id": "1111111111111111111",
                "video_url": "https://www.tiktok.com/@yanatheartist/video/7344527455798988074",
                "author_url": tiktok_videos.TIKTOK_AUTHOR_URL + "yanatheartist",
                "text": (
                    "Please use the filter. And this sound. Or send gifts directly to"
                    " focuscongo.com (@Pappy Orion) or @Friends of the Congo #goviralgo #newtrend"
                    " #viral "
                ),
                "like_count": 51500,
                "share_count": 2567,
                "impression_count": 761200,
                "collect_count": 2750,
                "comment_count": 713,
                "timestamp_filter": datetime.datetime(
                    2024, 3, 10, 0, 37, 59, tzinfo=datetime.timezone.utc
                ),
                "date_filter": datetime.date(2024, 3, 10),
                "year_filter": 2024,
                "month_filter": 3,
                "day_filter": 10,
                "file_timestamp": datetime.datetime(
                    2024, 1, 1, 4, 7, 46, 887000, tzinfo=datetime.timezone.utc
                ),
            },
        ]
    )
    return df


def test_videos_for_tagging(processed_videos_df):
    """Integration test for transforming processed videos ready for tagging (labelling) format."""
    result = tiktok_videos.for_tagging(processed_videos_df)
    expected = pd.DataFrame(
        [
            {
                "object_id": "1234567890000000000",
                "text": (
                    "Content Break- Tika started deleting all my comments 😃 #goviralgo"
                    " #beautyhacks #hudabeauty "
                ),
                "object_type": data_pull.constants.OBJECT_TYPE_TIKTOK_VIDEO,
                "created_at": datetime.datetime(
                    2024, 3, 10, 15, 30, 26, tzinfo=datetime.timezone.utc
                ),
                "object_url": "https://www.tiktok.com/@yanatheartist/video/7344757393005268267",
                "object_user_url": tiktok_videos.TIKTOK_AUTHOR_URL + "yanatheartist",
                "object_user_name": "1111111111111111111",
            },
            {
                "object_id": "9876543210000000000",
                "text": (
                    "Please use the filter. And this sound. Or send gifts directly to"
                    " focuscongo.com (@Pappy Orion) or @Friends of the Congo #goviralgo #newtrend"
                    " #viral "
                ),
                "object_type": data_pull.constants.OBJECT_TYPE_TIKTOK_VIDEO,
                "created_at": datetime.datetime(
                    2024, 3, 10, 0, 37, 59, tzinfo=datetime.timezone.utc
                ),
                "object_url": "https://www.tiktok.com/@yanatheartist/video/7344527455798988074",
                "object_user_url": tiktok_videos.TIKTOK_AUTHOR_URL + "yanatheartist",
                "object_user_name": "1111111111111111111",
            },
        ],
        index=pd.Index(
            ["1234567890000000000", "9876543210000000000"],
            dtype="object",
            name="object_id",
        ),
    )
    pd.testing.assert_frame_equal(result, expected)


def test_videos_data_pull(tiktok_raw_data_source_folder_url, processed_videos_df):
    """Integration test for the data pull.

    - Takes 2 test data source files
    - normalises them in to tiktok_videos schema
    - including adding the filter columns
    - de-duplicates them based on run datetime
    - order by created_at descending
    - add file_timestamp.
    """
    result = tiktok_videos.from_json(tiktok_raw_data_source_folder_url + "videos/")
    pd.testing.assert_frame_equal(result, processed_videos_df)
