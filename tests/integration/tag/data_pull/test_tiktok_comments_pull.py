"""Test Tiktok comment (threads) data puller."""
import datetime

import pandas as pd
import pytest

from phoenix.tag import data_pull


@pytest.fixture()
def processed_comments_df() -> pd.DataFrame:
    """Fixture for dataframe processed from scraped comment threads JSON."""
    return pd.DataFrame(
        [
            {
                "id": "comment_id_5",
                "created_at": datetime.datetime(
                    2024, 2, 16, 4, 35, 7, tzinfo=datetime.timezone.utc
                ),
                "updated_at": datetime.datetime(
                    2024, 2, 16, 4, 35, 7, tzinfo=datetime.timezone.utc
                ),
                "text": "comment_5-text_display",
                "like_count": 0,
                "is_top_level_comment": True,
                "total_reply_count": 0,
                "parent_comment_id": None,
                "author_id": "comment_5-author_id",
                "author_name": "comment_5-author_name",
                "author_url": data_pull.constants.TIKTOK_AUTHOR_URL + "comment_5-author_name",
                "video_id": "7332608030435020078",
                "video_url": "https://www.tiktok.com/@yanatheartist/video/7332608030435020078",
                "timestamp_filter": datetime.datetime(
                    2024, 2, 16, 4, 35, 7, tzinfo=datetime.timezone.utc
                ),
                "date_filter": datetime.date(2024, 2, 16),
                "year_filter": 2024,
                "month_filter": 2,
                "day_filter": 16,
                "file_timestamp": datetime.datetime(
                    2024, 3, 11, 6, 34, 25, 116000, tzinfo=datetime.timezone.utc
                ),
            },
            {
                "id": "comment_id_4",
                "created_at": datetime.datetime(
                    2024, 2, 8, 12, 40, 21, tzinfo=datetime.timezone.utc
                ),
                "updated_at": datetime.datetime(
                    2024, 2, 8, 12, 40, 21, tzinfo=datetime.timezone.utc
                ),
                "text": "comment_4-text_display",
                "like_count": 3,
                "is_top_level_comment": False,
                "total_reply_count": 0,
                "parent_comment_id": "comment_id_1",
                "author_id": "comment_4-author_id",
                "author_name": "comment_4-author_name",
                "author_url": data_pull.constants.TIKTOK_AUTHOR_URL + "comment_4-author_name",
                "video_id": "7332608030435020078",
                "video_url": "https://www.tiktok.com/@yanatheartist/video/7332608030435020078",
                "timestamp_filter": datetime.datetime(
                    2024, 2, 8, 12, 40, 21, tzinfo=datetime.timezone.utc
                ),
                "date_filter": datetime.date(2024, 2, 8),
                "year_filter": 2024,
                "month_filter": 2,
                "day_filter": 8,
                "file_timestamp": datetime.datetime(
                    2024, 3, 11, 6, 34, 25, 116000, tzinfo=datetime.timezone.utc
                ),
            },
            {
                "id": "comment_id_3",
                "created_at": datetime.datetime(
                    2024, 2, 7, 2, 54, 0, tzinfo=datetime.timezone.utc
                ),
                "updated_at": datetime.datetime(
                    2024, 2, 7, 2, 54, 0, tzinfo=datetime.timezone.utc
                ),
                "text": "comment_3-text_display",
                "like_count": 2,
                "is_top_level_comment": True,
                "total_reply_count": 2,
                "parent_comment_id": None,
                "author_id": "comment_3-author_id",
                "author_name": "comment_3-author_name",
                "author_url": data_pull.constants.TIKTOK_AUTHOR_URL + "comment_3-author_name",
                "video_id": "7332608030435020078",
                "video_url": "https://www.tiktok.com/@yanatheartist/video/7332608030435020078",
                "timestamp_filter": datetime.datetime(
                    2024, 2, 7, 2, 54, 0, tzinfo=datetime.timezone.utc
                ),
                "date_filter": datetime.date(2024, 2, 7),
                "year_filter": 2024,
                "month_filter": 2,
                "day_filter": 7,
                "file_timestamp": datetime.datetime(
                    2024, 1, 1, 1, 34, 25, 116000, tzinfo=datetime.timezone.utc
                ),
            },
            {
                "id": "comment_id_1",
                "created_at": datetime.datetime(
                    2024, 2, 6, 23, 45, 33, tzinfo=datetime.timezone.utc
                ),
                "updated_at": datetime.datetime(
                    2024, 2, 6, 23, 45, 33, tzinfo=datetime.timezone.utc
                ),
                "text": "comment_1-text_display",
                "like_count": 1,
                "is_top_level_comment": True,
                "total_reply_count": 1,
                "parent_comment_id": None,
                "author_id": "comment_1-author_id",
                "author_name": "comment_1-author_name",
                "author_url": data_pull.constants.TIKTOK_AUTHOR_URL + "comment_1-author_name",
                "video_id": "7332608030435020078",
                "video_url": "https://www.tiktok.com/@yanatheartist/video/7332608030435020078",
                "timestamp_filter": datetime.datetime(
                    2024, 2, 6, 23, 45, 33, tzinfo=datetime.timezone.utc
                ),
                "date_filter": datetime.date(2024, 2, 6),
                "year_filter": 2024,
                "month_filter": 2,
                "day_filter": 6,
                "file_timestamp": datetime.datetime(
                    2024, 3, 11, 6, 34, 25, 116000, tzinfo=datetime.timezone.utc
                ),
            },
        ]
    )


def test_comments_data_pull(tiktok_raw_data_source_folder_url, processed_comments_df):
    """Integration test for data pull (processing) of comments threads files.

    - Takes 2 test data source files
    - Cases include:
        - Comment that has non-zero reply count but no replies included in response
    - Normalises them
    - Includes adding year, month, date filter columns
    - Add file_timestamp
    - De-duplicates them based on comment ID and latest runtime file
    - Order by created_at descending
    """
    result = data_pull.tiktok_comments_pull.from_json(
        tiktok_raw_data_source_folder_url + "comment_threads/"
    )
    pd.testing.assert_frame_equal(result, processed_comments_df)


def test_comments_for_tagging(processed_comments_df):
    """Integration test for transforming processed comments ready for tagging (labelling)."""
    result = data_pull.tiktok_comments_pull.for_tagging(processed_comments_df)
    expected = pd.DataFrame(
        [
            {
                "object_id": "comment_id_5",
                "text": "comment_5-text_display",
                "object_type": "tiktok_comment",
                "created_at": datetime.datetime(
                    2024, 2, 16, 4, 35, 7, tzinfo=datetime.timezone.utc
                ),
                "object_url": "https://www.tiktok.com/@yanatheartist/video/7332608030435020078",
                "object_user_url": data_pull.constants.TIKTOK_AUTHOR_URL + "comment_5-author_name",
                "object_user_name": "comment_5-author_name",
                "object_parent_text": None,
            },
            {
                "object_id": "comment_id_4",
                "text": "comment_4-text_display",
                "object_type": "tiktok_comment",
                "created_at": datetime.datetime(
                    2024, 2, 8, 12, 40, 21, tzinfo=datetime.timezone.utc
                ),
                "object_url": "https://www.tiktok.com/@yanatheartist/video/7332608030435020078",
                "object_user_url": data_pull.constants.TIKTOK_AUTHOR_URL + "comment_4-author_name",
                "object_user_name": "comment_4-author_name",
                "object_parent_text": "comment_1-text_display",
            },
            {
                "object_id": "comment_id_3",
                "text": "comment_3-text_display",
                "object_type": "tiktok_comment",
                "created_at": datetime.datetime(
                    2024, 2, 7, 2, 54, 0, tzinfo=datetime.timezone.utc
                ),
                "object_url": "https://www.tiktok.com/@yanatheartist/video/7332608030435020078",
                "object_user_url": data_pull.constants.TIKTOK_AUTHOR_URL + "comment_3-author_name",
                "object_user_name": "comment_3-author_name",
                "object_parent_text": None,
            },
            {
                "object_id": "comment_id_1",
                "text": "comment_1-text_display",
                "object_type": "tiktok_comment",
                "created_at": datetime.datetime(
                    2024, 2, 6, 23, 45, 33, tzinfo=datetime.timezone.utc
                ),
                "object_url": "https://www.tiktok.com/@yanatheartist/video/7332608030435020078",
                "object_user_url": data_pull.constants.TIKTOK_AUTHOR_URL + "comment_1-author_name",
                "object_user_name": "comment_1-author_name",
                "object_parent_text": None,
            },
        ],
        index=pd.Index(
            ["comment_id_5", "comment_id_4", "comment_id_3", "comment_id_1"],
            dtype="object",
            name="object_id",
        ),
    )
    pd.testing.assert_frame_equal(result, expected)
