"""Test custom models.

This integration test shows and tests the dummy classifier.
"""
import pandas as pd

from phoenix import custom_models
from phoenix.custom_models import dummy_classifier, utils


def test_dummy_classifier(tmp_path):
    """Test dummy classifier."""
    objects_df = pd.DataFrame(
        {
            "object_id": [1, 2, 3],
        }
    )
    dummy_config = dummy_classifier.DEFAULT_DUMMY_CONFIG
    dummy_to_persist = dummy_classifier.DummyClassifier(dummy_config)
    _ = utils.persist_model(dummy_to_persist, dummy_classifier.DEFAULT_FILE_NAME, str(tmp_path))
    dummy = custom_models.get_model(
        model_key="dummy_classifier",
        base_custom_model_url=str(tmp_path),
    )

    predicted_df = dummy.predict(objects_df=objects_df)
    assert (predicted_df["dummy_class_1"] == dummy_config["dummy_class_1"]).all()
    assert (predicted_df["dummy_class_2"] == dummy_config["dummy_class_2"]).all()
