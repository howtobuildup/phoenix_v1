"""Test StaticPredictions custom model."""
import pandas as pd

from phoenix import custom_models
from phoenix.custom_models import static_predictions


def test_static_predictions(tmp_path):
    """Test StaticPredictions."""
    objects_df = pd.DataFrame(
        {
            "object_id": [1, 2, 3, 4],
        }
    )
    static_predictions_df = pd.DataFrame(
        {
            "object_id": [1, 2, 3],
            "some_class": [True, False, True],
            "some_other_class": [False, True, False],
            "non_bool_class": [1, 2, 3],
        }
    )
    static_predictions_df.to_csv(str(tmp_path / static_predictions.DEFAULT_FILE_NAME), index=False)
    static_predictions_model = custom_models.get_model(
        model_key="static_predictions",
        base_custom_model_url=str(tmp_path),
    )
    assert isinstance(static_predictions_model, static_predictions.StaticPredictions)
    assert static_predictions_model._static_predictions_df.equals(static_predictions_df)
    assert static_predictions_model.classification_columns() == ["some_class", "some_other_class"]

    predicted_df = static_predictions_model.predict(objects_df=objects_df)
    expected_df = pd.DataFrame(
        {
            "object_id": ["1", "2", "3", "4"],
            "some_class": [True, False, True, False],
            "some_other_class": [False, True, False, False],
        }
    )
    pd.testing.assert_frame_equal(predicted_df, expected_df)
