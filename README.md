# [Phoenix](https://howtobuildup.org/programs/digital-conflict/phoenix/)
Phoenix is a set of open source social media analysis tools.
These are the tools for the Phoenix project by [Build UP](https://howtobuildup.org).
There are four tools currently available:

* **Phoenix-gather:** used to scrape data from different social media (Twitter, Facebook, Instagram and YouTube)
* **Phoenix-tabulate:** used to move the data into a table, anonymise it, and organise it
* **Phoenix-classify:** used to add classes (labels) to data you have collected
* **Phoenix-visualise:** used to build a dashboard

# How to install and use
Currently, these tools are used predominantly by Build Up. To use these tools you can follow these two manuals
1. [Manual](https://docs.google.com/document/d/1nQTIpsAhtrI9pahpbaAdjnStzP7lCX4hIbWz_m804M4/edit).
2. [Practioner instructions](https://docs.google.com/document/d/123_DL-FaiXdyZVChW51owGGtTgRemBZ3ZbAdZHGTnh0/edit?usp=sharing).

Please be aware that every project needs a separate deployment. Currently, this is done by the datavaluepeople team. Feel free to contact them when a deployment is needed.

In the future Phoenix tools will be able to be run as a cloud deployment.

# Bug reports and feature requests
When encountering a bug please [create a GitLab issue](https://gitlab.com/howtobuildup/phoenix/-/issues/new) or create a merge request that fixes the bug.

If you want to discuss a specific deployment email [team@howtobuildup.org](mailto:team@howtobuildup.org).

# Organizations
<a href="https://howtobuildup.org">
    <img src="https://howtobuildup.org/wp-content/uploads/2021/04/build-up-logo.png" height="100">
</a>
<a href="http://datavaluepeople.com">
    <img src="https://howtobuildup.org/wp-content/uploads/2022/03/dvp.png" height="100">
</a>

# License
[GNU AGPLv3](/COPYING)