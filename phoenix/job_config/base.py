"""Base model for phoenix config."""
from typing import List, Union

from pydantic import BaseModel

from phoenix.job_config import gather


class Tasks(BaseModel):
    """Tasks object."""

    tasks: List[Union[gather.GatherApify, gather.GatherPlatform]]


class Environment(BaseModel):
    """Environment object."""

    gather: dict


class JobConfig(BaseModel):
    """JobConfig object."""

    deployment_id: str
    config_hash: str
    phoenix_config_version: str
    gather: Tasks
    environment: Environment
