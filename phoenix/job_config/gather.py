"""Gather object config."""
from typing import Optional

import datetime
from enum import Enum

from pydantic import BaseModel


class InputConfigInputType(str, Enum):
    """Input type enum."""

    accounts = "accounts"
    messages = "messages"
    threads = "threads"


class GatherBasePlatform(str, Enum):
    """Gather base platform enum."""

    facebook = "facebook"


class GatherBaseOutputType(str, Enum):
    """Gather base output type enum."""

    threads = "threads"
    messages = "messages"
    replies = "replies"


class GatherBaseInterval(str, Enum):
    """Gather base interval enum."""

    daily = "daily"
    weekly = "weekly"
    monthly = "monthly"


class MethodArgs(BaseModel):
    """Method args base model."""


class InputConfig(BaseModel):
    """Input config for a tasks."""

    path: str
    type: str
    input_type: InputConfigInputType


class GatherBase(BaseModel):
    """Gather base model."""

    source: str
    platform: GatherBasePlatform
    output_type: GatherBaseOutputType
    input_config: InputConfig
    start_date: datetime.date
    end_date: datetime.date
    interval: GatherBaseInterval
    limit_total_messages: Optional[int]
    limit_messages_per_account: Optional[int]
    limit_total_replies: Optional[int]
    limit_replies_per_message: Optional[int]
    sort_order: Optional[str]
    nested_replies: Optional[bool]
    method_args: Optional[MethodArgs] = None


class ApifyMethodArgs(MethodArgs):
    """Method args for Apify."""

    apify_api_key: str


class PlatformMethodArgs(MethodArgs):
    """Method args for Platform."""

    platform_api_key: str


class GatherApify(GatherBase):
    """Gather object for Apify."""

    source: str = "apify"
    method_args: ApifyMethodArgs


class GatherPlatform(GatherBase):
    """Gather object for Platform."""

    source: str = "platform_api"
    method_args: PlatformMethodArgs
