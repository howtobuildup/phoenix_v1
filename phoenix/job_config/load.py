"""Loads the config file and returns a config object."""
from pydantic_yaml import parse_yaml_raw_as

from phoenix.job_config import base


def load_from_local_file(config_file_path) -> base.JobConfig:
    """Loads the yaml config file and returns a config object."""
    yaml_file = open(config_file_path, "r")
    config = parse_yaml_raw_as(base.JobConfig, yaml_file)

    return config
