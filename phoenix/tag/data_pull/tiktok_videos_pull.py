"""Reading scraped tiktok data and normalising it into a dataframe."""
from typing import Any, Dict, List, Optional

import logging

import pandas as pd

from phoenix.tag.data_pull import constants, utils


logger = logging.getLogger(__name__)


JSONType = Any


TIKTOK_AUTHOR_URL = constants.TIKTOK_AUTHOR_URL


def from_json(
    url_to_folder: str, year_filter: Optional[int] = None, month_filter: Optional[int] = None
) -> pd.DataFrame:
    """Get all the JSON files of scraped data and return a normalised tiktok videos dataframe."""
    json_objects = utils.get_jsons(url_to_folder)

    dfs: List[pd.DataFrame] = []
    for json_object, file_timestamp in json_objects:
        df = create_dataframe(json_object)
        df["file_timestamp"] = file_timestamp
        dfs.append(df)

    df = utils.concat_dedupe_sort_objects(dfs, "created_at")
    df = utils.filter_df(df, year_filter, month_filter)
    return df


def create_dataframe(json_obj: List[Dict[str, Any]]) -> pd.DataFrame:
    """Create a normalised dataframe from json objects."""
    df = pd.json_normalize(json_obj)
    df = df.rename(
        columns={
            "id": "id",
            "createTimeISO": "created_at",
            "authorMeta.id": "author_id",
            "webVideoUrl": "video_url",
            "text": "text",
            "diggCount": "like_count",
            "shareCount": "share_count",
            "playCount": "impression_count",
            "collectCount": "collect_count",
            "commentCount": "comment_count",
        }
    )

    df["author_url"] = TIKTOK_AUTHOR_URL + df["authorMeta.name"].astype(str)

    df = df[
        [
            "id",
            "created_at",
            "author_id",
            "video_url",
            "author_url",
            "text",
            "like_count",
            "share_count",
            "impression_count",
            "collect_count",
            "comment_count",
        ]
    ]

    df = df.dropna(subset=["created_at"])
    df["created_at"] = pd.to_datetime(df["created_at"], utc=True)
    for col in ["like_count", "share_count", "impression_count", "collect_count", "comment_count"]:
        df[col] = df[col].astype(int)

    return utils.add_filter_cols(df, df["created_at"])


def for_tagging(given_df: pd.DataFrame):
    """Get Tiktok videos for tagging.

    Return:
    dataframe  : pandas.DataFrame
    Index:
        object_id: String, dtype: string
    Columns:
        object_id: String, dtype: string
        text: String, dtype: string
        object_type: "tiktok_videos", dtype: String
        created_at: datetime
        object_url: String, dtype: string
        object_user_url: String, dtype: string
        object_user_name: String, dtype: string
    """
    df = given_df.copy()
    df = df.rename(columns={"id": "object_id"})
    df["object_type"] = constants.OBJECT_TYPE_TIKTOK_VIDEO
    df["object_url"] = df["video_url"]
    df["object_user_url"] = df["author_url"]
    df["object_user_name"] = df["author_id"]
    df = df[
        [
            "object_id",
            "text",
            "object_type",
            "created_at",
            "object_url",
            "object_user_url",
            "object_user_name",
        ]
    ]
    df = df.set_index("object_id", drop=False, verify_integrity=True)
    return df
