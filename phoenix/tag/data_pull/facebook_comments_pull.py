"""Data pulling for facebook comments."""
from typing import Optional

import datetime
import itertools
import json
import logging

import pandas as pd
import tentaclio

from phoenix.tag.data_pull import constants, utils


def from_json(
    url_to_folder: str,
    objects_after: Optional[datetime.datetime] = None,
    objects_before: Optional[datetime.datetime] = None,
) -> pd.DataFrame:
    """Get all the jsons and return a normalised facebook comments."""
    comment_li = []
    for entry in tentaclio.listdir(url_to_folder):
        logging.info(f"Processing file: {entry}")
        if not utils.is_valid_file_name(entry):
            logging.info(f"Skipping file with invalid filename: {entry}")
            continue
        file_timestamp = utils.get_file_name_timestamp(entry)
        with tentaclio.open(entry) as file_io:
            pages = json.load(file_io)
            comments_df = get_comments_df(pages)

        comments_df["file_timestamp"] = file_timestamp
        comments_df["file_id"] = entry
        comment_li.append(comments_df)

    df = pd.concat(comment_li, axis=0, ignore_index=True)
    df = df.sort_values("file_timestamp")
    df = df.groupby("id").last()
    df = df.reset_index()
    df = normalise_comments_dataframe(df)
    if objects_after:
        df = df[df["timestamp_filter"] > objects_after]

    if objects_before:
        df = df[df["timestamp_filter"] < objects_before]

    return df


def get_comments_df(pages):
    """Get the comments dataframe from pages."""
    if "source" in pages[0] and pages[0]["source"] == "exportcomments":
        comments = list(([normalise_exportcomments(comment) for comment in pages]))
    # Apify facebook comments doesn't have a source key. feedbackId is a key
    # that none of the other comment sources have.
    elif "feedbackId" in pages[0]:
        comments = list(
            [
                normalise_apify_facebook(comment)
                for comment in pages
                if normalise_apify_facebook(comment) is not None
            ]
        )
    else:
        comments = list(itertools.chain.from_iterable([get_comments(page) for page in pages]))
    df = pd.DataFrame(comments)
    df = df[df["id"].astype(bool)]
    return df


def get_comments(page_json):
    """Get comments from page."""
    # In an early version of the comment parser the output artifact
    # had json within a json. This was a bug that has now been fixed.
    # Here we are still supporting the processing of the legacy format.
    if isinstance(page_json, str):
        page = json.loads(page_json)
    else:
        page = page_json
    return [normalise_comment(comment, page) for comment in page["comments"]]


def legacy_normalise_comment(comment, page):
    """Legacy Normalise comment."""
    return {
        "id": comment["fb_id"],
        "post_id": comment["top_level_post_id"],
        "file_id": comment["file_id"],
        "parent_id": comment["parent"],
        "post_created": comment["date_utc"],
        "text": comment["text"],
        "reactions": 0 if comment["reactions"] == "" else comment["reactions"],
        "top_sentiment_reactions": comment["sentiment"],
        "user_display_name": comment["display_name"],
        "user_name": comment["username"],
        "position": comment["position"],
    }


def normalise_comment(comment, page):
    """Normalise comment."""
    if "fb_id" in comment:
        return legacy_normalise_comment(comment, page)
    return {
        "id": comment["id"],
        "post_id": comment["post_id"],
        "file_id": comment["file_id"],
        "parent_id": comment["parent"],
        "post_created": comment["date_utc"],
        "text": comment["text"],
        "reactions": 0 if comment["reactions"] == "" else comment["reactions"],
        "top_sentiment_reactions": comment["top_sentiment_reactions"],
        "user_display_name": comment["user_display_name"],
        "user_name": comment["user_name"],
        "position": comment["position"],
    }


def normalise_exportcomments(comment):
    """Normalise comment created with ExportComments."""
    if "Comment ID" in comment:
        return normalise_exportcomments_instagram(comment)
    else:
        return normalise_exportcomments_facebook(comment)


def normalise_exportcomments_instagram(comment):
    """Normalise instagram comment created with ExportComments."""
    return {
        "id": comment["Comment ID"],
        "post_id": comment["post_id"],
        "file_id": comment["post_id"],
        "parent_id": comment["post_id"],
        "post_created": comment["Date"],
        "text": comment["Comment"],
        # ExportComments does not export reactions(likes) for instagram comments it is always 0.
        "reactions": None,
        "top_sentiment_reactions": None,
        "user_display_name": comment["Name"],
        "user_name": comment["Profile ID"],
        "position": "",
    }


def normalise_exportcomments_facebook(comment):
    """Normalise facebook comment created with ExportComments."""
    return {
        "id": utils.extract_comment_id(comment["URL"]),
        "post_id": comment["post_id"],
        "file_id": comment["post_id"],
        "parent_id": comment["post_id"],
        "post_created": comment["Date"],
        "text": comment["Comment"],
        "reactions": comment["Likes"],
        "top_sentiment_reactions": None,
        "user_display_name": comment["Name"],
        "user_name": comment["Profile ID"],
        "position": "",
    }


def normalise_apify_facebook(comment):
    """Normalise a facebook comment crawled from Apify."""
    # json sometimes has a comment without a facebookId, we skip these.
    if "facebookId" not in comment or "id" not in comment or "text" not in comment:
        return None
    parent_id = comment["facebookId"]
    position = ""
    # Currently there's a bug in the apify json crawler that doesn't return the replyToCommentId
    # field. A bugreport is open, and once this is fixed we can uncomment the next line.
    # parent_id = (
    #     comment["replyToCommentId"] if comment["replyToCommentId"] else comment["facebookId"]
    # )
    # position = comment["threadingDepth"]
    #
    return {
        "id": comment["id"],
        "post_id": comment["facebookId"],
        "parent_id": parent_id,
        "post_created": comment["date"],
        "text": comment.get("text", ""),  # apify removes the text key when it is an empty string.
        "reactions": comment["likesCount"],
        "top_sentiment_reactions": None,
        "user_display_name": comment["profileName"],
        "user_name": comment["profileId"],
        "position": position,
    }


def normalise_comments_dataframe(df):
    """Normalise the comments data frame."""
    # Not all ids are ints, some are strings.
    df["id"] = df["id"].astype(str)
    df["post_id"] = df["post_id"].astype(str)
    df["file_id"] = df["file_id"].astype(str)
    df["parent_id"] = df["parent_id"].astype(str)
    df["post_created"] = pd.to_datetime(df["post_created"]).dt.tz_convert("UTC")
    df["timestamp_filter"] = df["post_created"]
    df["date_filter"] = df["post_created"].dt.date
    df["year_filter"] = df["post_created"].dt.year
    df["month_filter"] = df["post_created"].dt.month
    df["day_filter"] = df["post_created"].dt.day
    df["text"] = df["text"].astype(str)
    df["reactions"] = df["reactions"].astype("Int64")
    df["user_display_name"] = df["user_display_name"].astype(str)
    df["user_name"] = df["user_name"].astype(str)
    df["position"] = df["position"].astype(str)
    return df


def for_tagging(given_df: pd.DataFrame):
    """Get facebook posts for tagging.

    Return:
    dataframe  : pandas.DataFrame
    Index:
        object_id: String, dtype: string
    Columns:
        object_id: String, dtype: string
        text: String, dtype: string
        object_type: "facebook_post", dtype: String

    """
    df = given_df.copy()
    df = df[["id", "text"]]
    df = df.rename(columns={"id": "object_id"})
    df = df.set_index("object_id", drop=False, verify_integrity=True)
    df["object_type"] = constants.OBJECT_TYPE_FACEBOOK_COMMENT
    return df
