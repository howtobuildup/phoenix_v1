"""Data pulling for twitter."""
from typing import Optional

import logging

import pandas as pd
import tentaclio

from phoenix.tag.data_pull import constants, utils


logger = logging.getLogger(__name__)

READ_DTYPES = {
    # Needed so that the ids
    # don't get converted to floats
    "id": str,
    "text": str,
    "conversation_id": str,
    "author_id": str,
    # These values are not in the json
    # dict if the tweet is not a reply
    "in_reply_to_user_id": object,
    "entities": object,
}


def twitter_json(
    url_to_folder: str, year_filter: Optional[int] = None, month_filter: Optional[int] = None
) -> pd.DataFrame:
    """Get all the csvs and return a dataframe with tweet data."""
    li = []
    for entry in tentaclio.listdir(url_to_folder):
        logger.info(f"Processing file: {entry}")
        file_timestamp = utils.get_file_name_timestamp(entry)
        with tentaclio.open(entry) as file_io:
            df = pd.read_json(file_io, dtype=READ_DTYPES)
            li.append(df)

        # Guard against empty files
        if df.shape[0] == 0:
            continue

        df["file_timestamp"] = file_timestamp

    df = pd.concat(li, axis=0, ignore_index=True)
    df = df.sort_values("file_timestamp")
    df = df.groupby("id").last()
    df = df.reset_index()
    df = normalise_json(df)
    if year_filter:
        df = df[df["year_filter"] == year_filter]
    if month_filter:
        df = df[df["month_filter"] == month_filter]
    return df


def normalise_json(raw_df: pd.DataFrame):
    """normalise_tweets raw dataframe."""
    df = raw_df
    df["in_reply_to_user_id"] = df["in_reply_to_user_id"].replace({pd.NA: None})
    df["timestamp_filter"] = df["created_at"]
    df["date_filter"] = df["created_at"].dt.date
    df["year_filter"] = df["created_at"].dt.year
    df["month_filter"] = df["created_at"].dt.month
    df["day_filter"] = df["created_at"].dt.day
    # These values are made NaN but they are not numerical
    # As such they are converted to None
    df["entities"] = df["entities"].replace({pd.NA: None})
    df["referenced_tweets"] = df["referenced_tweets"].replace({pd.NA: None})
    df = public_metrics_normalise(df)
    # Some of the medium type and deterimants are not possible with data from the v2 of the API

    df = add_medium_type_and_determinants(df)
    df = df.rename(columns={"lang": "language_from_api"})
    df["retweeted"] = df["referenced_tweets"].astype(bool)
    df = stringify_columns(df)
    # Dropping nested data for the moment
    df = df.drop(
        columns=[
            "entities",
            "edit_history_tweet_ids",
            "context_annotations",
            "public_metrics",
        ],
        errors="ignore",
    )
    return df


def public_metrics_normalise(raw_df: pd.DataFrame) -> pd.DataFrame:
    """Normalise public metrics."""
    metric_columns = [
        "retweet_count",
        "reply_count",
        "like_count",
        "quote_count",
        "bookmark_count",
        "impression_count",
    ]

    norm_df = pd.json_normalize(raw_df["public_metrics"])
    norm_df = norm_df[metric_columns]
    return raw_df.join(norm_df)


def get_medium_type_and_determinants(row: pd.Series) -> str:
    """Map the medium type and it's determinants."""
    type_of_first_media = get_type_of_first_media(row)
    url_of_first_entity = get_url_of_first_entity(row)
    medium_type = get_medium_type(type_of_first_media, url_of_first_entity)

    return pd.Series([medium_type, type_of_first_media, url_of_first_entity])


def get_medium_type(type_of_first_media: Optional[str], url_of_first_entity: Optional[str]) -> str:
    """Get the medium_type."""
    if type_of_first_media == "video":
        return constants.MEDIUM_TYPE_VIDEO

    if type_of_first_media in ["photo", "animated_gif"]:
        return constants.MEDIUM_TYPE_PHOTO

    if url_of_first_entity:
        return constants.MEDIUM_TYPE_LINK

    return constants.MEDIUM_TYPE_TEXT


def get_type_of_first_media(row: pd.Series) -> Optional[str]:
    """Get the media of the first media."""
    includes = {}
    if "includes" in row:
        includes = row["includes"]

    media = includes.get("media")
    type_of_first_media = None
    if media and len(media) > 0:
        type_of_first_media = media[0].get("type")

    return type_of_first_media


def get_url_of_first_entity(row: pd.Series) -> Optional[str]:
    """Get the urls of the first entity."""
    entities = {}
    if "entities" in row:
        entities = row["entities"]

    # Using pd.isnull as if the value is NaN
    # the `not entities` will return True
    if pd.isnull(entities):
        return None

    urls = entities.get("urls")
    url_of_first_url = None
    if urls and len(urls) > 0:
        url_of_first_url = urls[0].get("url")

    return url_of_first_url


def add_medium_type_and_determinants(df: pd.DataFrame) -> pd.DataFrame:
    """Add the medium_type and it's determinants to the dataframe."""
    medium_type_series = df.apply(get_medium_type_and_determinants, axis=1)
    df[["medium_type", "platform_media_type", "url_within_text"]] = medium_type_series
    return df


def for_tagging(given_df: pd.DataFrame):
    """Get tweets for tagging.

    Return:
    dataframe  : pandas.DataFrame
    Index:
        object_id: String, dtype: string
    Columns:
        object_id: String, dtype: string
        text: String, dtype: string
        object_type: "tweet", dtype: String
        created_at: datetime
        object_url: String, dtype: string
        object_user_url: String, dtype: string
        object_user_name: String, dtype: string

    """
    df = given_df.copy()
    df = df[["id", "text", "language_from_api", "created_at", "author_id"]]
    df["object_user_url"] = constants.TWITTER_URL + df["author_id"].astype(str)
    df["object_url"] = constants.TWITTER_URL + "tweet/status/" + df["id"]
    if "retweeted" in given_df.columns:
        df["retweeted"] = given_df["retweeted"]

    df = df.rename(columns={"id": "object_id", "author_id": "object_user_name"})
    df = df.set_index("object_id", drop=False, verify_integrity=True)
    df["object_type"] = constants.OBJECT_TYPE_TWEET
    return df


def stringify_columns(df: pd.DataFrame) -> pd.DataFrame:
    """Stringify columns that could contain dicts or lists."""
    for col in ["withheld_in_countries", "place", "coordinates"]:
        if col in df.columns:
            df[col] = df[col].fillna("")
            df[col] = df[col].astype("string")
    return df
