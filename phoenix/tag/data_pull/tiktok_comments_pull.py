"""Reading scraped tiktok comment (thread) data and normalising it into a dataframe."""
from typing import Any, Dict, List, Optional

import logging

import pandas as pd

from phoenix.tag.data_pull import constants, utils


logger = logging.getLogger(__name__)


JSONType = Any


TIKTOK_AUTHOR_URL = constants.TIKTOK_AUTHOR_URL


def from_json(
    url_to_folder: str,
    year_filter: Optional[int] = None,
    month_filter: Optional[int] = None,
    ignore_year_month_filters: Optional[bool] = False,
) -> pd.DataFrame:
    """Get all the JSON files of scraped data and return a normalised tiktok comment_thread df."""
    json_objects = utils.get_jsons(url_to_folder)

    dfs: List[pd.DataFrame] = []
    for json_object, file_timestamp in json_objects:
        df = create_dataframe(json_object)
        df["file_timestamp"] = file_timestamp
        dfs.append(df)

    df = utils.concat_dedupe_sort_objects(dfs, "created_at")
    df = utils.filter_df(df, year_filter, month_filter)
    return df


def create_dataframe(json_obj: List[Dict[str, Any]]) -> pd.DataFrame:
    """Create a normalised comment_threads dataframe from json objects."""
    df = pd.json_normalize(json_obj)
    df = df.dropna(subset=["createTimeISO"])
    df = df.rename(
        columns={
            "videoWebUrl": "video_url",
            "cid": "id",
            "createTimeISO": "created_at",
            "text": "text",
            "diggCount": "like_count",
            "repliesToId": "parent_comment_id",
            "replyCommentTotal": "total_reply_count",
            "uid": "author_id",
            "uniqueId": "author_name",
        }
    )
    df["video_id"] = df["video_url"].str.split("/").str[-1]
    df["id"] = df["id"].astype(str)
    df["author_url"] = TIKTOK_AUTHOR_URL + df["author_name"].astype(str)
    df["created_at"] = pd.to_datetime(df["created_at"], utc=True)
    df["updated_at"] = df["created_at"]
    df["like_count"] = df["like_count"].astype(int)
    df["is_top_level_comment"] = ~df["total_reply_count"].isna()
    df["total_reply_count"] = df["total_reply_count"].fillna(0.0).astype(int)

    df = df[
        [
            "id",
            "created_at",
            "updated_at",
            "text",
            "like_count",
            "is_top_level_comment",
            "total_reply_count",
            "parent_comment_id",
            "author_id",
            "author_name",
            "author_url",
            "video_id",
            "video_url",
        ]
    ]

    return utils.add_filter_cols(df, df["created_at"])


def for_tagging(given_df: pd.DataFrame):
    """Get Tiktok comments for tagging.

    Return:
    dataframe  : pandas.DataFrame
    Index:
        object_id: ID of object from source system/platform, dtype: string
    Columns:
        object_id: ID of object from source system/platform, dtype: string
        text: Text content of the object, dtype: string
        object_type: Type i.e. class of the object, dtype: String
        created_at: Date time when object was created/posted dtype: datetime
        object_url: URL to location of object, dtype: string
        object_user_url: URL to creator/author of object, if available dtype: Optional[string]
        object_user_name: User name of creator/author, dtype: string
        object_parent_text: Text of the parent object, if applicable, dtype: Optional[string]
    """
    df = given_df.copy()
    df = df.merge(
        df[["id", "text"]].rename(columns={"id": "_id", "text": "parent_comment_text"}),
        how="left",
        left_on="parent_comment_id",
        right_on="_id",
    )
    df = df.rename(
        columns={
            "id": "object_id",
            "parent_comment_text": "object_parent_text",
            "author_url": "object_user_url",
            "video_url": "object_url",
            "author_name": "object_user_name",
        }
    )
    df["object_type"] = constants.OBJECT_TYPE_TIKTOK_COMMENT
    df = df[
        [
            "object_id",
            "text",
            "object_type",
            "created_at",
            "object_url",
            "object_user_url",
            "object_user_name",
            "object_parent_text",
        ]
    ]
    df = df.set_index("object_id", drop=False, verify_integrity=True)
    return df
