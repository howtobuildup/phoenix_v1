"""Init topic."""
from itertools import chain

import pandas as pd


def get_object_topics(topics_df, objects_df):
    """Get the objects topic dataframe.

    Parameters:
    topics_df:
        object_id: object_id
        object_type: object_type
        topic: string
        matched_features: List[string]
    objects_df:
        see docs/schemas/objects.md

    Returns:
        objects topics: see docs/schemas/objects.md#Object_topics
    """
    df = topics_df.groupby(["object_id", "object_type"])
    has_topics_df = (
        topics_df[topics_df["has_topic"].isin([True])]
        .groupby(["object_id", "object_type"])
        .first()
        .rename(columns={"has_topic": "has_topics"})
    )
    df = df.agg({"topic": list}).rename(columns={"topic": "topics"})
    df = df.join(has_topics_df["has_topics"]).fillna(False)
    objects_df = objects_df.set_index("object_id", drop=False)
    df = df.droplevel(["object_type"])
    df = objects_df.join(df)
    return df.reset_index(drop=True)


def merge_topics(topics_dfs=list[pd.DataFrame]) -> pd.DataFrame:
    """Merge multiple topics dataframes.

    This will handle the case where objects in one topics dataframe has a topic and does not in
    the other. It will then remove the fill topic.

    Args:
        topics_dfs: List of topics dataframes.

    Returns:
        topics_df: topics dataframe.
    """
    has_topics_list = [df[df["has_topic"].isin([True])] for df in topics_dfs]
    has_topics_df = pd.concat(has_topics_list)
    has_topics_df = has_topics_df.groupby(
        ["object_id", "object_type", "topic", "has_topic"], as_index=False
    ).agg({"matched_features": lambda x: list(chain.from_iterable(x))})
    has_topics_df = has_topics_df.sort_values(["object_id", "object_type", "topic"])

    has_not_topics_list = [df[df["has_topic"].isin([False])] for df in topics_dfs]
    has_not_topics_df = pd.concat(has_not_topics_list).drop_duplicates(
        ["object_id", "object_type", "topic"]
    )

    has_not_topics_df = has_not_topics_df[
        ~has_not_topics_df["object_id"].isin(has_topics_df["object_id"])
    ]

    result_df = pd.concat([has_topics_df, has_not_topics_df]).reset_index(drop=True)
    # Reorder the columns
    return result_df[["object_id", "topic", "object_type", "matched_features", "has_topic"]]
