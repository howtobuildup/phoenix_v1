"""Scrape Facebook comments."""
import click

from phoenix.common import run_params
from phoenix.common.cli_modules import scrape_group, utils


@scrape_group.scrape.command("facebook_comments_raw_csv_process")
@click.argument("artifact_env")
@click.argument("tenant_id")
@click.option(
    "--base_url_facebook_comments_csv_to_parse",
    default=None,
    help=("The path to the directory where the csv files are stored with the comments."),
)
@click.argument("month_offset", type=click.INT, default=0)
def facebook_comments_raw_csv_process(
    artifact_env,
    tenant_id,
    base_url_facebook_comments_csv_to_parse,
    month_offset,
):
    """Process the csv export of facebook comments from third-party scrapers.

    Example command:
    ./phoenix-cli scrape facebook_comments production tenant

    Or, to process comments from a custom location:
    ./phoenix-cli scrape facebook_comments production tenant
    --custom_file_path src/local_artifacts/facebook_comments/

    ARTIFACT_ENV:
        The artifact environment that will be used. Default "local"
        Can use "production" which will pick the artifact env from the env var.
        Or a valid storage URL like "s3://my-phoenix-bucket/"
    TENANT_ID: The id of the tenant to run phoenix for.
    """
    cur_run_params = run_params.general.create(artifact_env, tenant_id)
    year_filter, month_filter = utils.get_year_month_for_offset(
        cur_run_params.run_dt, month_offset
    )
    extra_parameters = {
        "ARTIFACT_SOURCE_FB_COMMENTS_URL": cur_run_params.art_url_reg.get_url(
            "source-facebook_comments"
        ),
        "OBJECT_TYPE": "facebook_comments",
        "YEAR_FILTER": year_filter,
        "MONTH_FILTER": month_filter,
    }
    parameters = {
        **utils.init_parameters(cur_run_params),
        **extra_parameters,
    }
    if base_url_facebook_comments_csv_to_parse:
        parameters[
            "BASE_URL_FACEBOOK_COMMENTS_CSV_TO_PARSE"
        ] = base_url_facebook_comments_csv_to_parse

    input_nb_url = utils.get_input_notebook_path("scrape/facebook_comments_csv_parse.ipynb")
    output_nb_url = cur_run_params.art_url_reg.get_url(
        "source-facebook_comments_csv_parse_notebook"
    )

    utils.run_notebooks(input_nb_url, output_nb_url, parameters)
