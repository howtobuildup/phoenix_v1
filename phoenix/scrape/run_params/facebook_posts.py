"""RunParams facebook posts."""
from typing import Any, Dict, List, Optional, Union

import dataclasses
import datetime

from phoenix.common.config import tenant
from phoenix.common.run_params import base, general
from phoenix.scrape import crowdtangle
from phoenix.scrape.run_params import utils


DEFAULT_SCRAPE_SINCE_DAYS = 3


@dataclasses.dataclass
class FacebookPostsScrapeRunParamsURLs(base.RunParams):
    """URLS."""

    config: Dict[str, Any]
    source: str


@dataclasses.dataclass
class FacebookPostsScrapeRunParams(base.RunParams):
    """Finalise accounts run params."""

    urls: FacebookPostsScrapeRunParamsURLs
    general: general.GeneralRunParams
    scrape_since_days: Optional[int]
    scrape_end_date: datetime.datetime
    scrape_start_date: datetime.datetime
    crowdtangle_list_ids: List[str]


def create(
    artifacts_environment_key: str,
    tenant_id: str,
    run_datetime_str: Optional[str],
    scrape_since_days: Optional[Union[str, int]] = None,
    scrape_start_date: Optional[Union[datetime.datetime, str]] = None,
    scrape_end_date: Optional[Union[datetime.datetime, str]] = None,
    crowdtangle_list_ids: Optional[Union[List[str], str]] = None,
) -> FacebookPostsScrapeRunParams:
    """Create FacebookPostsScrapeRunParams."""
    general_run_params = general.create(artifacts_environment_key, tenant_id, run_datetime_str)

    art_url_reg = general_run_params.art_url_reg

    crowdtangle_list_ids = get_crowdtangle_list_ids(
        general_run_params.tenant_config, crowdtangle_list_ids
    )

    url_config: Dict[str, Any] = {}
    urls = FacebookPostsScrapeRunParamsURLs(
        config=url_config,
        source=art_url_reg.get_url("source-posts", url_config),
    )
    scrape_since_days, scrape_start_date, scrape_end_date = utils.get_scraping_range(
        general_run_params,
        DEFAULT_SCRAPE_SINCE_DAYS,
        scrape_since_days,
        scrape_start_date,
        scrape_end_date,
    )
    return FacebookPostsScrapeRunParams(
        general=general_run_params,
        urls=urls,
        scrape_since_days=scrape_since_days,
        scrape_start_date=scrape_start_date,
        scrape_end_date=scrape_end_date,
        crowdtangle_list_ids=crowdtangle_list_ids,
    )


def get_crowdtangle_list_ids(
    tenant_config: tenant.TenantConfig,
    crowdtangle_list_ids: Optional[Union[List[str], str]] = None,
) -> List[str]:
    """Get the crowdtangle_list_ids."""
    if isinstance(crowdtangle_list_ids, str):
        crowdtangle_list_ids = crowdtangle.process_scrape_list_id(crowdtangle_list_ids)
    elif isinstance(crowdtangle_list_ids, list):
        crowdtangle_list_ids = crowdtangle_list_ids
    elif tenant_config.crowdtangle_scrape_list_id:
        crowdtangle_list_ids = crowdtangle.process_scrape_list_id(
            tenant_config.crowdtangle_scrape_list_id
        )

    if not crowdtangle_list_ids:
        raise ValueError("CrowdTangle List Ids to scrape is needed")

    return crowdtangle_list_ids
