"""Utils of scrape run_params."""
from typing import Optional, Tuple, Union

import datetime

from phoenix.common.run_params import general


DATE_FORMAT = "%Y-%m-%d"


def parse_scrape_date(date_str: str) -> datetime.datetime:
    """Parse the string into a date."""
    dt = datetime.datetime.strptime(date_str, DATE_FORMAT)
    dt = dt.replace(tzinfo=datetime.timezone.utc)
    return dt


def get_scraping_range(
    general_run_params: general.GeneralRunParams,
    default_scrape_since_days: int,
    scrape_since_days: Optional[Union[str, int]] = None,
    scrape_start_date: Optional[Union[datetime.datetime, str]] = None,
    scrape_end_date: Optional[Union[datetime.datetime, str]] = None,
) -> Tuple[Union[int, None], datetime.datetime, datetime.datetime]:
    """Get the scraping range."""
    if scrape_since_days and scrape_start_date:
        raise ValueError("Cannot set scrape_since_days and scrape_start_date.")

    if scrape_since_days or scrape_since_days == 0:
        scrape_since_days = int(scrape_since_days)
    else:
        scrape_since_days = default_scrape_since_days

    if isinstance(scrape_end_date, str):
        scrape_end_date = parse_scrape_date(scrape_end_date)

    if not scrape_end_date:
        scrape_end_date = general_run_params.run_dt.dt

    if isinstance(scrape_start_date, str):
        scrape_start_date = parse_scrape_date(scrape_start_date)

    if scrape_start_date:
        scrape_since_days = None
    else:
        scrape_start_date = scrape_end_date - datetime.timedelta(days=int(scrape_since_days))

    return (scrape_since_days, scrape_start_date, scrape_end_date)
