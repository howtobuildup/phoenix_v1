"""RunParams tweets."""
from typing import Any, Dict, Optional, Union

import dataclasses
import datetime

from phoenix.common.run_params import base, general
from phoenix.scrape.run_params import utils


DEFAULT_SCRAPE_SINCE_DAYS = 3
VALID_QUERY_TYPES = ["user", "keyword"]


@dataclasses.dataclass
class TweetsScrapeRunParamsURLs(base.RunParams):
    """URLS that will be used in the scraping of tweets."""

    config: Dict[str, Any]
    source: str
    static_queries_users: str
    static_queries_keywords: str


@dataclasses.dataclass
class TweetsScrapeRunParams(base.RunParams):
    """Parameters that will be used in the scraping of the tweets."""

    urls: TweetsScrapeRunParamsURLs
    general: general.GeneralRunParams
    scrape_since_days: Optional[int]
    scrape_end_date: datetime.datetime
    scrape_start_date: datetime.datetime
    query_type: str = "user"
    limit_per_query: Optional[int] = None


def create(
    artifacts_environment_key: str,
    tenant_id: str,
    query_type: str,
    run_datetime_str: Optional[str],
    artifact_source_tweets_url: Optional[str] = None,
    static_url_queries_users: Optional[str] = None,
    static_url_queries_keywords: Optional[str] = None,
    scrape_since_days: Optional[Union[str, int]] = None,
    scrape_start_date: Optional[Union[datetime.datetime, str]] = None,
    scrape_end_date: Optional[Union[datetime.datetime, str]] = None,
    limit_per_query: Optional[Union[int, str]] = None,
) -> TweetsScrapeRunParams:
    """Create TweetsScrapeRunParams.

    Args:
        artifacts_environment_key: str.
        tenant_id: str. Must be a valid tenant_id.
        query_type: str, "user", "keyword"
        run_datetime_str: str, datetime in format "%Y-%m-%d %H:%M:%S"
        artifact_source_tweets_url: str, override the url to the source of the tweets.
        static_url_queries_users: str, override the default url to the static user timeline queries
        static_url_queries_keywords: str, override the default url to the static keyword queries.
        scrape_since_days: int, number of days to scrape can't be used if scrape_start_date or
            scrape_end_date are used.
        scrape_start_date: datetime.datetime, start date to scrape.
        scrape_end_date: datetime.datetime, end date to scrape.

    Returns:
        TweetsScrapeRunParams
    """
    if query_type not in VALID_QUERY_TYPES:
        raise ValueError(f"Invalid query_type: {query_type}")
    general_run_params = general.create(artifacts_environment_key, tenant_id, run_datetime_str)

    art_url_reg = general_run_params.art_url_reg

    url_config: Dict[str, Any] = {}
    if not artifact_source_tweets_url:
        mapper_key = f"source-{query_type}_tweets"
        source = art_url_reg.get_url(mapper_key, url_config)
    else:
        source = artifact_source_tweets_url

    if not static_url_queries_users:
        static_url_queries_users = art_url_reg.get_url("static-twitter_users", url_config)

    if not static_url_queries_keywords:
        static_url_queries_keywords = art_url_reg.get_url("static-twitter_keywords", url_config)

    if not static_url_queries_users or not static_url_queries_keywords:
        raise ValueError("No static_url_queries.")

    urls = TweetsScrapeRunParamsURLs(
        config=url_config,
        source=source,
        static_queries_users=static_url_queries_users,
        static_queries_keywords=static_url_queries_keywords,
    )
    scrape_since_days, scrape_start_date, scrape_end_date = utils.get_scraping_range(
        general_run_params,
        DEFAULT_SCRAPE_SINCE_DAYS,
        scrape_since_days,
        scrape_start_date,
        scrape_end_date,
    )

    if limit_per_query is not None:
        limit_per_query = int(limit_per_query)
    return TweetsScrapeRunParams(
        query_type=query_type,
        general=general_run_params,
        urls=urls,
        scrape_since_days=scrape_since_days,
        # API does not allow to scrape tweets with microsecond
        scrape_start_date=scrape_start_date.replace(microsecond=0),
        scrape_end_date=scrape_end_date.replace(microsecond=0),
        limit_per_query=limit_per_query,
    )
