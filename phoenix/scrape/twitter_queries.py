"""Twitter query connections through the Twitter API."""
from typing import Any, Dict

import datetime
import logging
import os
import time
from collections.abc import Iterator
from math import inf

import tweepy


# User authentication OAuth 1
ENV_CONSUMER_KEY = "TWITTER_CONSUMER_KEY"
ENV_CONSUMER_SECRET = "TWITTER_CONSUMER_SECRET"
ENV_OAUTH_ACCESS_TOKEN = "TWITTER_OAUTH_ACCESS_TOKEN"
ENV_OAUTH_ACCESS_SECRET = "TWITTER_OAUTH_ACCESS_SECRET"

# Bearer token
ENV_BEARER_TOKEN = "TWITTER_BEARER_TOKEN"


TWEET_FIELDS = [
    "id",
    "text",
    "edit_history_tweet_ids",
    "created_at",
    "public_metrics",
    "lang",
    "author_id",
    "conversation_id",
    "entities",
    "context_annotations",
    "in_reply_to_user_id",
    "possibly_sensitive",
    "referenced_tweets",
    "withheld",
]


def get_client() -> tweepy.Client:
    """Get twitter client for v2 API.

    See docs/scraping/twitter.md for more information.
    """
    default_params = {
        "wait_on_rate_limit": True,
    }
    bearer_token = os.getenv(ENV_BEARER_TOKEN)
    if bearer_token:
        logging.info("Using OAuth2BearerHandler for tweepy authentication.")
        return tweepy.Client(bearer_token, **default_params)

    consumer_key = os.getenv(ENV_CONSUMER_KEY)
    consumer_secret = os.getenv(ENV_CONSUMER_SECRET)
    access_token = os.getenv(ENV_OAUTH_ACCESS_TOKEN)
    access_secret = os.getenv(ENV_OAUTH_ACCESS_SECRET)
    if consumer_key and consumer_secret:
        logging.info("Using OAuth 1.0a User Context for tweepy authentication.")
        client = tweepy.Client(
            consumer_key=consumer_key,
            consumer_secret=consumer_secret,
            access_token=access_token,
            access_token_secret=access_secret,
            **default_params,
        )
        return client

    raise RuntimeError("Authentication for Twitter is not correctly configured.")


def _get_tweet_iterator(
    client, params, query_type: str, start_time: datetime.datetime, limit=None
) -> Iterator[Dict[str, Any]]:
    """Selects the correct cursor function and extracts data into an iterator."""
    if query_type == "user":
        return _get_user_tweets(client, params, limit)
    elif query_type == "keyword" and need_all_tweets_endpoint(start_time):
        return _get_search_all_tweets(client, params, limit)
    elif query_type == "keyword":
        return _get_search_recent_tweets(client, params, limit)
    else:
        logging.info("Bad query.")
        raise ValueError(f"Invalid query_type: {query_type}")


def need_all_tweets_endpoint(start_time: datetime.datetime) -> bool:
    """Check if we need to use the search_all_tweets endpoint.

    Args:
        start_time: datetime.datetime

    Returns:
        bool: True if the difference between the current time and the start_time is more than 7
            days, thus needing to use the search_all_tweets endpoint.
    """
    if datetime.datetime.now(tz=datetime.timezone.utc) - start_time > datetime.timedelta(days=7):
        return True
    else:
        return False


def _get_user_tweets(client, params, limit=None) -> Iterator[Dict[str, Any]]:
    """Manages the cursor for Twitter client.user_timeline endpoint and extracts data."""
    if limit:
        # Using max_results of 5 because it is the minimum allowed. Each page will be limited to
        # 5 tweets, and each of those 5 tweets will count towards the limit. If the limit is
        # higher than 5, the paginator will request a maximum of limit + 4 tweets (such as if
        # you ask for a limit of 11).
        return tweepy.Paginator(client.get_users_tweets, max_results=5, **params).flatten(
            limit=limit
        )
    else:
        return tweepy.Paginator(client.get_users_tweets, max_results=100, **params).flatten()


def _get_search_recent_tweets(client, params, limit=None) -> Iterator[Dict[str, Any]]:
    """Manages the cursor for Twitter client.search_recent_tweets endpoint and extracts data."""
    if limit:
        # Using max_results of 10 because it is the minimum allowed for this endpoint. Each page
        # will be limited to 10 tweets, but will iterate to get the limit.
        return tweepy.Paginator(client.search_recent_tweets, max_results=10, **params).flatten(
            limit=limit
        )
    else:
        return tweepy.Paginator(client.search_recent_tweets, max_results=100, **params).flatten()


def _get_search_all_tweets(client, params, limit=None) -> Iterator[Dict[str, Any]]:
    """Manages the cursor for Twitter client.search_all_tweets endpoint and extracts data.

    Note: this endpoint is only available to academic researchers (or pro users), although recent
    documentation has removed how to apply for access, and what the requirements are.
    see https://developer.twitter.com/en/docs/twitter-api/getting-started/about-twitter-api
    """
    if limit:
        # Using max_results of min(limit, 100) since if you have the access to the endpoint you
        # can request quite a lot of tweets and you want to minimize the number of requests (
        # and thus time taken ) to get that limit instead of minimizing the excess number of
        # tweets returned. The minimum is 10, so setting that as minimum.
        max_results = max(min(limit, 100), 10)
        return flatten_rate_limited(
            tweepy.Paginator(client.search_all_tweets, max_results=max_results, **params),
            limit=limit,
        )
    else:
        return flatten_rate_limited(
            tweepy.Paginator(client.search_all_tweets, max_results=100, **params)
        )


def get_tweets(
    query_type,
    query,
    start_time: datetime.datetime,
    end_time: datetime.datetime,
    limit=None,
    client=None,
) -> tweepy.Paginator:
    """Get the tweet paginator that will request for tweets from the Client.

    Beaware that if limit is used it will return the limit but it may request more tweets than the
    limit. This is because the limit is done in the paginator after the tweets have been returned.
    The paginator will request a maximum of tweets that equals limit + 4 but will return the limit.

    Args:
        query_type (str): "user"
        query (str): The query to search for.
        start_time (datetime.datetime): The start time of the query.
        end_time (datetime.datetime): The end time of the query.
        limit (int, optional): The maximum number of tweets to return. Defaults to None.
        client (tweepy.Client, optional): The tweepy client. Defaults to None.

    Raises:
        ValueError: If query_type is not "user".

    Returns:
        tweepy.Paginator: The paginator for the query.
    """
    # Check Client
    if not client:
        client = get_client()
    # Check query type
    logging.info(f"Query_type: {query_type} | Query: {query}")
    params = {
        "start_time": start_time.isoformat(),
        "end_time": end_time.isoformat(),
        "tweet_fields": ",".join(TWEET_FIELDS),
    }
    if query_type == "user":
        params["id"] = query
    elif query_type == "keyword":
        params["query"] = query

    tweet_iterator = _get_tweet_iterator(client, params, query_type, start_time, limit)

    return tweet_iterator


def get_user_ids(queries, client=None) -> list[str]:
    """Get user_id from username."""
    if not client:
        client = get_client()
    response = client.get_users(usernames=queries)

    return [user.id for user in response.data]


def get_tweets_for_queries(
    query_type,
    queries,
    start_time: datetime.datetime,
    end_time: datetime.datetime,
    limit_per_query=None,
    client=None,
    queries_is_ids=False,
) -> list[tweepy.Tweet]:
    """Get the tweets for all the queries.

    Args:
        query_type: "user"
        queries: list of queries, either usernames, user_ids, or keywords.
        start_time: datetime.datetime
        end_time: datetime.datetime
        limit: int
        client: tweepy.Client
        queries_is_ids: bool, if True, queries are user_ids, else usernames and user_ids will be
            retrieved from the Client

    Returns:
        list[tweepy.Tweet]: The list of tweets.
    """
    if query_type not in ["user", "keyword"]:
        raise ValueError(f"Query type {query_type} not supported.")

    tweets = []
    if not client:
        client = get_client()

    if query_type == "user" and not queries_is_ids:
        queries = get_user_ids(queries, client)

    for query in queries:
        for tweet in get_tweets(query_type, query, start_time, end_time, limit_per_query, client):
            tweets.append(tweet)
    return tweets


def flatten_rate_limited(
    paginator, sleep_time: float = 3.1, limit=inf
) -> Iterator[Dict[str, Any]]:
    """Flatten paginated data with rate limit handling that isn't in the x-rate-limit header.

    Copied from the tweepy source code for Paginator.flatten() and modified to add a sleep
    time between requests.
    https://github.com/tweepy/tweepy/blob/f32d12dbddbd877470446657812a10a04292d0c9/tweepy
    /pagination.py#L55

    paginator: tweepy.Paginator
    sleep_time: float, time to sleep between requests. Default 3.1 seconds as the rate limit is
        300 requests per 15 minutes
    limit (int): Maximum number of results to yield
    """
    if limit <= 0:
        return

    count = 0
    for response in paginator:
        if isinstance(response, tweepy.client.Response):
            response_data = response.data or []
        elif isinstance(response, dict):
            response_data = response.get("data", [])
        else:
            raise RuntimeError(
                f"Paginator.flatten does not support the {type(response)} "
                f"return type of {paginator.func.__name__}"
            )
        for data in response_data:
            yield data
            count += 1
            if count == limit:
                # Add sleep time after the last request to ensure there won't be a <1 second gap
                # between the end of one keyword and the start of the next.
                time.sleep(sleep_time)
                return
        # Only change from the original flatten function are these sleep times
        time.sleep(sleep_time)
