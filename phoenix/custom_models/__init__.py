"""Custom Models.

This module contains the custom models used in the Phoenix pipeline. The
custom models are used to classify objects in the pipeline. The custom
models can be built in any way the developer see's fit, but they must adhere to the CustomModel
interface (protocol) in custom_models/typing.py.

The easiest way to make a new model is:
    1. Copy the folder dummy_classifier/ to a new folder with the name of your model.
    2. Change the new files you have created to match your model. Make sure that there is good
       documentation on how to train and create a new model.
"""
from functools import partial

import pandas as pd

from phoenix.common.run_params import general
from phoenix.custom_models import dummy_classifier, static_predictions, typing, utils
from phoenix.tag.topic import single_feature_match


# Dictionary that is used to create the models based on the key in the tenant.yaml
MODEL_DICT: dict[str, typing.CREATE_MODEL_FUNCTION_CALLABLE] = {
    # This key is the value that must be in `tenant.yaml`
    "dummy_classifier": dummy_classifier.load,
    "static_predictions": static_predictions.load,
}


def get_model(model_key: str, base_custom_model_url: str) -> typing.CustomModel:
    """Gets a model with the model key.

    Args:
        model_key: (str) the key of the model in the MODEL_DICT
        base_custom_model_url: (str) base url for the model
    Returns:
        model: (typing.CustomModel) loaded model
    """
    if model_key not in MODEL_DICT:
        raise ValueError(
            f"{model_key} is not a valid custom model. Valid models are: {MODEL_DICT.keys()}"
        )

    return MODEL_DICT[model_key](base_custom_model_url)


def create_custom_classifiers_dict(
    custom_models_config: list[dict[str, str]], base_custom_model_url: str
) -> dict[str, typing.CustomModel]:
    """Creates models for all custom classifiers.

    Args:
        class_names: (list[dict[str, str]]) list of configurations for custom models
        base_custom_model_url: (str) base url for the model

    Returns:
        classifiers_dict: (dict[str, typing.CustomModel]) Dictionary of Models
    """
    classifiers_dict = {}
    for config in custom_models_config:
        if "key" not in config:
            raise ValueError("Custom model config must have a key.")
        key = config["key"]
        classifiers_dict[key] = get_model(key, base_custom_model_url)
    return classifiers_dict


def run_predict(
    classifiers_dict: dict[str, typing.CustomModel],
    objects_df: pd.DataFrame,
) -> dict[str, pd.DataFrame]:
    """Runs predict on all models in model_dict.

    Args:
        model_dict: (dict[str, typing.CustomModel]) Dictionary of Models
        objects_df: (pd.DataFrame) objects_df

    Returns:
        predictions_dict: (dict[str, pd.DataFrame]) Dictionary of Predictions
    """
    predictions_dict = {}
    for classifier_name, classifier_model in classifiers_dict.items():
        predictions_dict[classifier_name] = classifier_model.predict(objects_df)

    return predictions_dict


def predictions_to_topics_df(
    objects_df: pd.DataFrame,
    classifiers_dict: dict[str, typing.CustomModel],
    predictions_dict: dict[str, pd.DataFrame],
) -> pd.DataFrame:
    """Transforms predictions into topics.

    Joins all the data from the objects_df to the data in each DataFrame in predictions_dict on the
    "object_id" column. The columns that are joined are configured by the `classification_columns`
    method on each classifier.

    Parameters:
    objects_df:
        see docs/schemas/objects.md
    classifiers_dict: A dictionary of CustomModels with the following methods:
        classification_class: returns a list of columns
    predictions_dict: A dictionary of DataFrames with the following columns:
        object_id: object_id
        object_type: object_type
        f"is_{some_class}": boolean
        f"{some_class}": boolean

    Returns:
        result_df: A DataFrame with the following schema of topics.md
    """
    topics_dfs = []

    for key, predictions_df in predictions_dict.items():
        classification_columns = classifiers_dict[key].classification_columns()
        if not set(classification_columns).issubset(predictions_df.columns):
            raise ValueError(
                f"Columns in {classification_columns} not in {predictions_df.columns}"
            )
        df = predictions_df.set_index("object_id")
        df = df[classification_columns]
        df = df.melt(ignore_index=False)
        df = df.rename(columns={"variable": "topic"})
        df = df[df["value"].isin([True])]
        df = df.drop(columns=["value"])
        # Although this is not strictly a matched feature it is helpful to know which model
        # predicted the topic. It also allows for duplicates between different models to be seen in
        # the matched features column.
        df["matched_features"] = f"custom_model_{key}"
        topics_dfs += [df.reset_index()]

    topics_df = pd.concat(topics_dfs)
    topics_df = topics_df.groupby(["object_id", "topic"], as_index=False).agg(
        {"matched_features": list}
    )
    topics_df = topics_df.set_index("object_id")
    objects_to_join = objects_df[["object_id", "object_type"]]
    objects_to_join = objects_to_join.set_index("object_id")
    result_df = topics_df.join(objects_to_join, how="right")
    result_df["has_topic"] = result_df["topic"].notnull()
    result_df["topic"] = result_df["topic"].fillna(single_feature_match.FILL_TOPIC)
    result_df = result_df.sort_values(by=["object_id", "topic"])
    return result_df.reset_index()
