"""StaticPredictions Model.

This model is used for applying a one off classification that uses a model that can be run as part
of the pipeline.

The use cases are:
    - Model that is too big to run in the pipeline
    - Model that is too slow to run in the pipeline

Solution:
    - Export all the data that should be classified
    - Classify this data using the model
    - export the result of this model
    - persist these results as a static file in
        `base_custom_model_url`/`DEFAULT_FILE_NAME`
    - Configure `static_predictions` as a custom model for the tenant
    - Run `tagging` with custom_models inference. (see ./phoenix-cli help)

Be aware:
    - Data format of persist predictions should be:
        - object_id,<classification_columns>
        - Where object_id matches the id of the exported data
        - <classification_columns> can have any name and be any amount
            but must be type bool
    - The pipeline can still be run monthly but the static_predictions.csv must contain
        the results for all the data that is should be classifer (ie across months).
"""
import logging

import pandas as pd

from phoenix.custom_models import utils


logger = logging.getLogger(__name__)

DEFAULT_FILE_NAME = "static_predictions.csv"


class StaticPredictions:
    """StaticPredictions Model."""

    _static_predictions_df: pd.DataFrame

    def __init__(self, static_predictions_df: pd.DataFrame) -> None:
        """Init.

        Args:
            static_predictions_df: static predictions dataframe
        """
        self._static_predictions_df = static_predictions_df

    def predict(self, objects_df: pd.DataFrame) -> pd.DataFrame:
        """Predict.

        Args:
            objects_df: objects to make predictions about

        Returns:
            predictions
        """
        columns_to_merge = ["object_id"] + self.classification_columns()
        to_merge_df = self._static_predictions_df[columns_to_merge]
        to_merge_df["object_id"] = to_merge_df["object_id"].astype(str)
        objects_df["object_id"] = objects_df["object_id"].astype(str)
        return objects_df.merge(to_merge_df, on="object_id", how="left").fillna(False)

    def classification_columns(self) -> list[str]:
        """Get classification columns.

        Returns:
            a list of columns that are the classifications
        """
        columns = list(self._static_predictions_df.columns)
        return [
            column
            for column in columns
            if column != "object_id" and self._static_predictions_df[column].dtype == bool
        ]


def load(
    base_custom_model_url: str,
) -> StaticPredictions:
    """Load StaticPredictions.

    Args:
        base_custom_model_url: base url for custom models

    Returns:
        StaticPredictions
    """
    url = utils.create_model_url(DEFAULT_FILE_NAME, base_custom_model_url)
    if not url.endswith(".csv"):
        raise ValueError(f"Model url must end with .csv, got {url}")
    static_predictions_df = pd.read_csv(url)
    return StaticPredictions(static_predictions_df=static_predictions_df)
