"""Utils for custom models."""
from typing import Type

import pickle

import numpy as np
import pandas as pd
import tentaclio

from phoenix.common import artifacts
from phoenix.custom_models import typing


def explode_str(df: pd.DataFrame, col: str, sep: str):
    """Explodes columns that have strings separated by a `sep` into new rows.

    Args:
        df: (pd.DataFrame) dataframe in which you want to explode a column
        col: (str) column name that you want to explode
        sep: (str) separator that signifies it needs to be exploded
    """
    s = df[col]
    i = np.arange(len(s)).repeat(s.str.count(sep) + 1)
    df = df.iloc[i].assign(**{col: sep.join(s).split(sep)})
    df[col] = df[col].str.lstrip()
    return df


def load_model(
    expected_model_class: Type[typing.CustomModel], file_name: str, base_custom_model_url: str
) -> typing.CustomModel:
    """Loads a custom model from a file.

    This function is a general function that can be used to load a custom model that has been
    pickled.

    Args:
        expected_model_class: (Type[typing.CustomModel]) expected model class
        file_name: (str) name of the file to be loaded
        base_custom_model_url: (str) base url for the custom model
    Returns:
        loaded_model: (typing.CustomModel) loaded model
    """
    model_url = create_model_url(file_name, base_custom_model_url)
    _ = model_url_default_guard(model_url)
    with tentaclio.open(model_url, "rb") as f:
        loaded_model = pickle.load(f)

    if not isinstance(loaded_model, expected_model_class):
        raise TypeError(
            f"Loaded model is not of type {expected_model_class}, got {type(loaded_model)}"
        )

    return loaded_model


def persist_model(model: typing.CustomModel, file_name: str, base_custom_model_url: str) -> str:
    """Persists a custom model to a file.

    Args:
        model: (typing.CustomModel) model to be persisted
        file_name: (str) name of the file to be persisted
        base_custom_model_url: (str) base url for the custom model

    Returns:
        model_url: (str) url of the persisted model
    """
    model_url = create_model_url(file_name, base_custom_model_url)
    _ = model_url_default_guard(model_url)
    artifacts.utils.create_folders_if_needed(model_url)
    with tentaclio.open(model_url, "wb") as f:
        pickle.dump(model, f)
    return model_url


def model_url_default_guard(model_url: str) -> str:
    """Default guard for model_url.

    Args:
        model_url: (str) model url

    Returns:
        model_url: (str) model url
    """
    if not model_url.endswith(".pkl"):
        raise ValueError(f"Model url must end with .pkl, got {model_url}")
    return model_url


def create_model_url(file_name: str, base_custom_model_url: str) -> str:
    """Creates a model url.

    Args:
        file_name: (str) name of the file to be persisted
        base_custom_model_url: (str) base url for the custom model

    Returns:
        model_url: (str) url of the persisted model
    """
    if not base_custom_model_url.endswith("/"):
        base_custom_model_url += "/"
    return f"{base_custom_model_url}{file_name}"
