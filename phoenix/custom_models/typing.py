"""Init custom_models submodule."""
from typing import Callable, Protocol

import pandas as pd


class CustomModel(Protocol):
    """Protocol for the CustomModel."""

    def predict(self, objects_df: pd.DataFrame) -> pd.DataFrame:
        """Predicts the class based on config.

        This method should be implemented in the custom model and should add the classification
        columns as dtype bool to the objects_df.

        The columns must match that of the `classification_columns` method.

        Args:
            objects_df (pd.DataFrame): DataFrame with objects.

        Returns:
            pd.DataFrame: DataFrame with predictions.
        """
        ...

    def classification_columns(self) -> list[str]:
        """The columns that the classifier produces.

        This is needed for the `has_classes` columns to know which columns should be considered.

        Returns:
            list[str]: The columns that the classifier produces.
        """
        ...


CREATE_MODEL_FUNCTION_CALLABLE = Callable[[str], CustomModel]
