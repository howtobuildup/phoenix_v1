"""Dummy classifier.

This classifier is used for testing purposes and as an example for building new models.

To train the model use `./train_and_persist_dummy_classifier.ipynb`.
"""
from typing import Any

import pandas as pd

from phoenix.custom_models import utils


DEFAULT_DUMMY_CONFIG = {
    "dummy_class_1": True,
    "dummy_class_2": False,
}

DEFAULT_FILE_NAME = "dummy_classifier.pkl"


class DummyClassifier:
    """Dummy classifier."""

    config: dict[str, Any]

    def __init__(self, config):
        self.config = config

    def predict(self, objects_df: pd.DataFrame) -> pd.DataFrame:
        """Predicts the class based on config."""
        df = objects_df.copy()
        for key, value in self.config.items():
            df[key] = value

        return df

    def classification_columns(self) -> list[str]:
        """The columns that the classifier produces.

        This is needed for the `has_classes` columns to know which columns should be considered.

        Returns:
            list[str]: The columns that the classifier produces.
        """
        return list(self.config.keys())


# Although this could have been done as a partial:
# `load = partial(utils.load_model, DummyClassifier, DEFAULT_FILE_NAME)`
# Using a verbose `load` function hopefully makes it clear that any `load` function can be used for
# a custom model.
def load(base_custom_model_url: str) -> DummyClassifier:
    """Loads the dummy classifier."""
    loaded_model = utils.load_model(DummyClassifier, DEFAULT_FILE_NAME, base_custom_model_url)
    # This guard is needed for the type checking
    if not isinstance(loaded_model, DummyClassifier):
        raise TypeError(f"Loaded model is not of type DummyClassifier, got {type(loaded_model)}")
    return loaded_model
